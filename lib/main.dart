import 'dart:async';
import 'dart:typed_data';
import 'package:firebase_core/firebase_core.dart';
import 'package:vecinos247/clases/datos_sesion.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/models/appdata_model.dart';
import 'package:vecinos247/src/pages/admin/drawer_admin.dart';
import 'package:vecinos247/src/pages/login/afterLogin.dart';
import 'package:vecinos247/src/pages/residente/drawer.dart';
import 'package:vecinos247/src/providers/perfilProvider.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'src/pages/login/Login.dart';
import 'dart:convert';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new MyApp(),
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [
        const Locale('en'), // English
        const Locale('es'), // Hebrew
        // Chinese *See Advanced Locales below*
        // ... other locales the app supports
      ],
    ));
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final perfilProvider = PerfilProvider();

  startTime() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, _validarSesionAbierta);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
  }

  @override
  void initState() {
    super.initState();

    startTime();
  }

  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Center(
                child: Container(
              width: MediaQuery.of(context).size.width * 0.8,
              child: new Image.asset('recursos/logo/logo.png'),
            )),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Container(
                  height: size.width * 0.1,
                  child: Image.asset('recursos/imagenes/LogoSF.png'),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _validarSesionAbierta() async {
    final prefs = await SharedPreferences.getInstance();
    String valores = prefs.getString("Informacion");
    // String valores = '{"resp":"ok","msj":"Bienvenido","id":2,"cedula":55555,"nombre_residente":"JUAN RULFO PARAMO","lista_subunidades":[{"id_unidad":32,"nombre_unidad":"PENHOUSE PARIS","url_pago":null,"id_subunidad":1,"nombre_subunidad":"APTO 505","nombre_tipo_unidad":"Apartamento","saldo":0,"perfiles":[{"id_perfil":100}],"token":"eyJpdiI6ImNqZUZMTlRrSnhDeUNUV2FEK3lBa1E9PSIsInZhbHVlIjoia3l2d3U2ajNsXC9hbXA1R1BVQzM3YkpzbDh4cnI3SHVPc2F3azBteUF2V1k9IiwibWFjIjoiYTYwNTlhOWFhYjQ5Y2QzOGJlYjE2MjNhZmNkNWFmNWM2OTI3NjIyMjZmZTMzN2Y3ZTMzODBmY2IyZTY3MzViOSJ9"},{"id_unidad":34,"nombre_unidad":"PENHOUSE JOSE","url_pago":null,"id_subunidad":2,"nombre_subunidad":"APTO 1304","nombre_tipo_unidad":"Apartamento","saldo":0,"perfiles":[{"id_perfil":100}],"token":"eyJpdiI6ImNqZUZMTlRrSnhDeUNUV2FEK3lBa1E9PSIsInZhbHVlIjoia3l2d3U2ajNsXC9hbXA1R1BVQzM3YkpzbDh4cnI3SHVPc2F3azBteUF2V1k9IiwibWFjIjoiYTYwNTlhOWFhYjQ5Y2QzOGJlYjE2MjNhZmNkNWFmNWM2OTI3NjIyMjZmZTMzN2Y3ZTMzODBmY2IyZTY3MzViOSJ9"}]}';
    print(valores);
    if (valores != null) {
      Map datos = jsonDecode(valores.trim());
      if (datos["token_app"] == null) {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
      }
      DatosApp datosApp = DatosApp.fromJson(datos);
      DatosSesion datosSesion = DatosSesion();
      datosSesion.sesionActiva(datosApp);
      if (appData.datosAppSelecionado.perfiles.length == 1) {
        if (appData.datosApp.lista_subunidades[0].perfiles[0].id_perfil ==
            100) {
          appData.datosAppSelecionado.permiso = "100";
          appData.rol = "Residente";
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            if (appData.rol == "Residente" || appData.rol == "Familiar") {
              return DrawerItem();
            } else {
              return DrawerAdminItem();
            }
          }));
        } else if (appData
                .datosApp.lista_subunidades[0].perfiles[0].id_perfil ==
            300) {
          appData.datosAppSelecionado.permiso = "300";
          appData.rol = "Admin";
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            if (appData.rol == "Residente" || appData.rol == "Familiar") {
              return DrawerItem();
            } else {
              return DrawerAdminItem();
            }
          }));
        } else {
          appData.datosAppSelecionado.permiso = "200";
          appData.rol = "Familiar";
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            if (appData.rol == "Residente" || appData.rol == "Familiar") {
              return DrawerItem();
            } else {
              return DrawerAdminItem();
            }
          }));
        }
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => AfterLoginPage()));
      }
    } else {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
    }
  }

/*LinearGradient(
            begin: Alignment.topRight, 
            end: Alignment.bottomRight,
            colors: [
               //Color.fromRGBO(205, 105, 55,1.0),
                Color.fromRGBO(240, 147, 43,1.0),
                Color.fromRGBO(255, 153, 29, 1.0),
               
               Color.fromRGBO(255, 135, 5,1.0),
              
               Color.fromRGBO(242, 84, 12,1.0),
              
              
              
              
               
               */
}
