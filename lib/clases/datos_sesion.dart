import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/models/appdata_model.dart';
import 'package:vecinos247/src/models/appdata_usuario_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DatosSesion {
  SharedPreferences _prefs;

  set informacionUsuario(String value) {
    _prefs.setString("datosLocal", value);
  }

  get getInformacionUsuario {
    return _prefs.getString("datosLocal");
  }

  void sesionActiva(DatosApp datosApp) {
    DatosAppSelecionado selected = DatosAppSelecionado();
    appData.datosApp = datosApp;
    appData.fotoPerfil = null;

    selected.cedula = datosApp.cedula;
    selected.id = datosApp.id;
    selected.nombre_residente = datosApp.nombre_residente;
    selected.id_subunidad = datosApp.lista_subunidades[0].id_subunidad;
    selected.id_unidad = datosApp.lista_subunidades[0].id_unidad;
    selected.nombre_unidad = datosApp.lista_subunidades[0].nombre_unidad;
    selected.nombre_subunidad = datosApp.lista_subunidades[0].nombre_subunidad;
    selected.nombre_tipo_unidad =
        datosApp.lista_subunidades[0].nombre_tipo_unidad;
    selected.url_pago = datosApp.lista_subunidades[0].url_pago;
    selected.saldo = datosApp.lista_subunidades[0].saldo;
    selected.token = datosApp.lista_subunidades[0].token;
    selected.perfiles = datosApp.lista_subunidades[0].perfiles;

    appData.datosAppSelecionado = selected;
  }

  void selectedUnidad(DatosAppSubUnidades subunidad) {
    DatosAppSelecionado selected = DatosAppSelecionado();
    for (int i = 0; i < subunidad.perfiles.length; i++) {
      if (subunidad.perfiles[i].id_perfil.toString() ==
          appData.datosAppSelecionado.permiso) {
        print("permisoooooo");
        print(appData.datosApp.nombre_residente);
        print(subunidad.perfiles[i].id_perfil.toString());
        selected.permiso = subunidad.perfiles[i].id_perfil.toString();
      }
    }
    selected.cedula = appData.datosApp.cedula;
    selected.id = appData.datosApp.id;
    selected.nombre_residente = appData.datosApp.nombre_residente;
    selected.id_subunidad = subunidad.id_subunidad;
    selected.id_unidad = subunidad.id_unidad;
    selected.nombre_unidad = subunidad.nombre_unidad;
    selected.nombre_subunidad = subunidad.nombre_subunidad;
    selected.nombre_tipo_unidad = subunidad.nombre_tipo_unidad;
    selected.url_pago = subunidad.url_pago;
    selected.saldo = subunidad.saldo;
    selected.token = subunidad.token;
    selected.perfiles = subunidad.perfiles;

    appData.datosAppSelecionado = selected;
  }
}
