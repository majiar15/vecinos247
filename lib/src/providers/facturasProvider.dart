import 'dart:io';

import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/facturas.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'package:dio/dio.dart' as dioClient;

class FacturaProvider {
  final String baseUrl = constantes.apiUrl;
  final String baseUrlDio = constantes.apiUrlDio;

  Future<DatosFacturas> getlistafactura(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "username": appData.datosAppSelecionado.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
    };
    print(authData);
    final response = await client.post("$baseUrl/factura/get/actual",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 200) {
      try {
        final decodedData = json.decode(response.body);
        final entidades = new DatosFacturas.fromJson(decodedData);
        return entidades;
      } catch (a) {
        return null;
      }
    }
    if (response.statusCode == 403) {
      desLogin(context);
    } else {
      return null;
    }
  }

  Future getSaldo() async {
    final Map<String, dynamic> authData = {
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    print(authData);
    final response = await client.post("$baseUrl/factura/saldo_total",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 200) {
      try {
        final decodedData = json.decode(response.body);
        print("======================================");
        print(authData);
        return decodedData;
      } catch (a) {
        return null;
      }
    } else if (response.statusCode == 403) {
      return 403;
    } else {
      return null;
    }
  }

  // Future<void> generarNoticia(BuildContext context, String titulo,
  //     String descripcion, String vencimiento, File image) async {
  //   // convertImage(image) {
  //   //   List<int> imageBytes = image.readAsBytesSync();
  //   //   String base64Image = base64.encode(imageBytes);
  //   //   print(base64Image);
  //   //   return base64Image;
  //   // }
  //   print("generarNoticia ===================================");
  //   dioClient.FormData authData;
  //   try {
  //     // si hay imagen no saltara error
  //     authData = new dioClient.FormData.fromMap({
  //       "username": appData.datosAppSelecionado.cedula,
  //       "InputTitulo": titulo,
  //       "InputDescripcion": descripcion,
  //       "InputImagen": await dioClient.MultipartFile.fromFile(image.path),
  //       "InputVencimiento": vencimiento,
  //       "InputEstado": 1,
  //       "token": appData.datosAppSelecionado.token
  //     });
  //   } catch (e) {
  //     // si hay error significa que no hay  imagen, entonces se asigna null
  //     authData = new dioClient.FormData.fromMap({
  //       "username": appData.datosAppSelecionado.cedula,
  //       "InputTitulo": titulo,
  //       "InputDescripcion": descripcion,
  //       "InputImagen": null,
  //       "InputVencimiento": vencimiento,
  //       "InputEstado": 1,
  //       "token": appData.datosAppSelecionado.token
  //     });
  //   }
  //   final url = Uri.http(baseUrlDio, '/api/admin/cartelera/save');
  //   var response = await dioClient.Dio().postUri(url,
  //       data: authData,
  //       options: dioClient.Options(headers: {
  //         'contentType': 'multipart/form-data',
  //         // set content-length
  //       }));
  //   print(response.data['resp']);
  // }
  Future<String> enviarFactura(
      BuildContext context, File image, int id_factura) async {
    ProgressDialog pr = new ProgressDialog(context);
    pr.style(
        message: 'enviando factura...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    dioClient.FormData authData;
    authData = new dioClient.FormData.fromMap({
      "id_factura": id_factura,
      "adjunto": await dioClient.MultipartFile.fromFile(image.path),
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    });
    final url = Uri.https(baseUrlDio, '/api/factura/adjunto/save');
    pr.show();
    final response = await dioClient.Dio().postUri(url,
        data: authData,
        options: dioClient.Options(headers: {
          'contentType': 'multipart/form-data',
          // set content-length
        }));
    pr.hide();
    print(response);
    print({
      "id_factura": id_factura,
      "adjunto": image,
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    });

    return response.data['resp'];
  }

  Future getLinkPago(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    final response = await client.post("$baseUrl/residente/link_pago",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 200) {
      try {
        final decodedData = json.decode(response.body);
        return decodedData["link_pago"];
      } catch (a) {
        return null;
      }
    } else if (response.statusCode == 403) {
      desLogin(context);
    } else {
      return null;
    }
  }

  Future getHistory(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    final response = await client.post("$baseUrl/factura/list/historico",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 200) {
      try {
        final decodedData = json.decode(response.body);

        return decodedData;
      } catch (a) {
        return null;
      }
    } else if (response.statusCode == 403) {
      desLogin(context);
    } else {
      return null;
    }
  }
}
