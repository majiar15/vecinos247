import 'package:flutter/cupertino.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/hijo.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;

class HijoProvider {
  final String baseUrl = constantes.apiUrl;

  Future<List<Hijo>> getlistahijo(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "cedula": appData.datosAppSelecionado.cedula,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    final response = await client.post("$baseUrl/nucleo_familiar/list/hijos",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
    if (response.statusCode == 403) {
      desLogin(context);
    }
    try {
      final decodedData = json.decode(response.body);
      final entidades = new ListaHijo.fromJsonList(decodedData);

      return entidades.items.reversed.toList();
    } catch (e) {
      return [];
    }
  }

  Future<Hijo> getHijo(String cedula) async {
    final Map<String, dynamic> authData = {
      "cedula_familiar": cedula,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    final response = await client.post("$baseUrl/familiar/check",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);

    Hijo datosHijos = Hijo();

    try {
      if (response.statusCode == 200) {
        final datos = json.decode(response.body);
        if (datos["resp"] == "error") {
          datosHijos.estado_solicitud = false;
          datosHijos.mensaje_solicitud = datos["msj"];
          return datosHijos;
        } else {
          final decodedData = json.decode(response.body);
          final hijodatos = new Hijo.fromJson(decodedData);
          hijodatos.estado_solicitud = true;
          hijodatos.nombre_familiar = decodedData['nombre'];
          return hijodatos;
        }
      } else {
        datosHijos.estado_solicitud = false;
        datosHijos.mensaje_solicitud = "Ocurrio un error";
        return datosHijos;
      }
    } catch (e) {
      datosHijos.estado_solicitud = false;
      datosHijos.mensaje_solicitud = "Ocurrio un error";
      return datosHijos;
    }
  }
}
