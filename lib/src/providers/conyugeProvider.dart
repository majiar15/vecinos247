import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/conyugue.dart';
import 'package:http/http.dart' as client;

class ConyugeProvider {
  final String baseUrl = constantes.apiUrl;

  Future<Conyuge> getConyuge(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "cedula": appData.datosAppSelecionado.cedula,
      "token": appData.datosAppSelecionado.token,
      "username": appData.datosAppSelecionado.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
    };
    final response = await client.post("$baseUrl/nucleo_familiar/get/conyuge",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    print(response.body);
    final decodedData = json.decode(response.body);
    final conyuge = new Conyuge.fromJson(decodedData);

    return conyuge;
  }
}
