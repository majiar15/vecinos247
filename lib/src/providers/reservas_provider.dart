import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/reserva_disponibilidad.dart';
import 'package:vecinos247/src/models/visitas_models.dart';
import 'package:http/http.dart' as client;
import 'package:vecinos247/src/models/zonaSocial.dart';

class ReservasProvider {
  final String baseUrl = constantes.apiUrl;

  Future<List<Reserva>> getAllReservas(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };

    final response = await client.post("$baseUrl/reserva/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    print(response.body);
    try {
      final decodedData = json.decode(response.body);
      final entidades = new Reservas.fromJsonList(decodedData);
      return entidades.items.reversed.toList();
    } catch (a) {
      return [];
    }
  }

  Future<List<Reserva>> getreservatodas(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };

    final response = await client.post("$baseUrl/reserva/list/all",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);

    try {
      final entidades = new Reservas.fromJsonList(decodedData);
      return entidades.items.reversed.toList();
    } catch (a) {
      return null;
    }
  }

  Future<List<Reserva>> getreservatodasAdmin() async {
    final Map<String, dynamic> authData = {
      "username": appData.datosApp.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };

    final response = await client.post("$baseUrl/admin/reserva/list/all",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    final decodedData = json.decode(response.body);
    print(response.body);
    try {
      final entidades = new Reservas.fromJsonList(decodedData);
      return entidades.items.reversed.toList();
    } catch (a) {
      return null;
    }
  }

  Future<String> setAprobarReserva(String id_reserva, String fecha) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "id_reserva": id_reserva,
      "fecha_hora_inicio": fecha,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };
    final response = await client.post("$baseUrl/admin/reserva/approve",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);

    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      try {
        if (datos['resp'] == "ok") {
          return datos['msj'];
        } else {
          return datos['msj'];
        }
      } catch (a) {
        return datos['Ocurrio un error'];
      }
    } else {
      return 'Ocurrio un error';
    }
  }

  Future<String> setCancelarReserva(
      String id_reserva, String fecha_hora_inicio, BuildContext context) async {
    ProgressDialog pr = new ProgressDialog(context);
    pr.style(
        message: 'Cancelando reserva...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "id_reserva": id_reserva,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "fecha_hora_inicio": fecha_hora_inicio
    };
    pr.show();
    final response = await client.post("$baseUrl/admin/reserva/deny",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    pr.hide();
    final datos = json.decode(response.body);
    if (response.statusCode == 200) {
      try {
        if (datos['resp'] == "ok") {
          return datos['msj'];
        } else {
          return datos['msj'];
        }
      } catch (a) {
        return datos['Ocurrio un error'];
      }
    } else {
      return datos['Ocurrio un error'];
    }
  }

  Future<List<DisponibilidadReserva>> getDisponibilidadReserva(
      BuildContext context, String fecha) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "fecha": fecha,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };
    final response = await client.post("$baseUrl/zonas/disponibilidad/porfecha",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final datos = json.decode(response.body);

    print(datos);

    if (response.statusCode == 200) {
      final entidades = new ListaDisponibilidadReserva.fromJsonList(datos);
      List<DisponibilidadReserva> listDisponibles = [];
      entidades.items.forEach((element) {
        try {
          if (element.disponibilidades != null &&
              element.disponibilidades.length != 1) {
            print(element.disponibilidades.length);
            listDisponibles.add(element);
          }
        } catch (e) {}
      });

      print("fin LOP");
      print(listDisponibles);

      return listDisponibles;
    } else {
      return null;
    }
  }
}
