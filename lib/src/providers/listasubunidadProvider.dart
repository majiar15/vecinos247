import 'package:flutter/widgets.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/listasubunidad.dart';
import 'dart:convert';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:http/http.dart' as client;

class ListaSubUnidadProvider {
  final String baseUrl = constantes.apiUrl;

  Future<List<DatosListaSubunidades>> getlistasubunidades(
      BuildContext context) async {
    final Map<String, dynamic> authData = {
      "cedula": appData.datosAppSelecionado.cedula,
      "username": appData.datosAppSelecionado.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token,
    };
    print(authData);

    final response = await client.post("$baseUrl/admin/subunidad/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    final decodedData = json.decode(response.body);
    print(response);
    if (response.statusCode == 403) {
      desLogin(context);
    }
    try {
      final entidades = new ListaSubunidades.fromJsonList(decodedData);
      return entidades.items.reversed.toList();
    } catch (a) {
      Future<List<DatosListaSubunidades>> a;
      return a;
    }
  }
}
