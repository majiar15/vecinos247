import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:http/http.dart' as client;
import 'dart:convert';

class LinkProvider {
  final String baseUrl = constantes.apiUrl;

  Future<String> setLink(String link) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "link_pago": link,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };
    final response = await client.post("$baseUrl/admin/factura/linkpago/save",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    final datos = json.decode(response.body);

    if (response.statusCode == 200) {
      try {
        if (datos['resp'] == "ok") {
          return datos['msj'];
        } else {
          return datos['msj'];
        }
      } catch (a) {
        return datos['Ocurrio un error'];
      }
    } else {
      return datos['Ocurrio un error'];
    }
  }
}
