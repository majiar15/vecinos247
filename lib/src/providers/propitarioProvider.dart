import 'package:flutter/cupertino.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/propietario.dart';
import 'package:http/http.dart' as client;
import 'dart:convert';

import '../constantes.dart';

class PropitarioProvider {
  final String baseUrl = constantes.apiUrl;
  Future<DatosPropietario> getPropitario(
      BuildContext context, String cedula) async {
    final Map<String, dynamic> authData = {
      "cedula": cedula,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula,
    };
    final response = await client.post(
        "$baseUrl/admin/subunidad/propietario/get",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);
    final propitario = new DatosPropietario.fromJson(decodedData);
    return propitario;
  }
}
