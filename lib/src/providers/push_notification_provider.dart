import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:vecinos247/src/constantes.dart';
import 'package:flushbar/flushbar.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:soundpool/soundpool.dart';

class PushNotificationProvider {
  Soundpool _soundpool = Soundpool();
  int _alarmSoundStreamId;
  int _cheeringStreamId = 1;

  Future<int> _soundId;
  Future<int> _cheeringId;

  final String baseUrl = constantes.apiUrl;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final _mensajesStreamController = StreamController<String>.broadcast();
  Stream<String> get mensajes => _mensajesStreamController.stream;

  Future<Map<String, dynamic>> postToken(token) async {
    final Map<String, dynamic> authData = {
      "id_residente": appData.datosAppSelecionado.id,
      "token_app": token,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };

    Client client = Client();
    final response = await client.post("$baseUrl/residente/token/save",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});

    print("Firebase Token - PostToken");
    print(response.body);
  }

  Future<Map<String, dynamic>> enviarPushChat(
      String title, String mensaje, int destinatarios, int perfil) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "token": appData.datosAppSelecionado.token,
      "asunto": title,
      "cuerpo": mensaje,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "remitente": appData.datosAppSelecionado.nombre_residente,
      "destinatarios": destinatarios,
      "perfil_remitente": perfil
    };
    print(authData);
    Client client = Client();
    final response = await client.post("$baseUrl/chat/save",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
    print(authData);
  }

  initNotifications(fun([String action]), context) {
    _soundId = _loadSound();
    _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging.getToken().then((token) {
      print("Firebase Token: " + token);
      postToken(token);
    });

    void _colorfullAlert(texto, f) {
      Flushbar(
          flushbarPosition: FlushbarPosition.TOP,
          dismissDirection: FlushbarDismissDirection.HORIZONTAL,
          message: texto,
          duration: Duration(seconds: 7),
          backgroundColor: Colors.orange,
          mainButton: FlatButton(
            onPressed: () {
              print("presionado: " + texto);
              f();
            },
            child: Text(
              'VER',
              style:
                  TextStyle(fontFamily: 'CenturyGothic', color: Colors.white),
            ),
          ))
        ..show(context);
    }

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        _playSound();
        String argumento = 'ugu';
        String title = '';

        if (Platform.isAndroid) {
          argumento = message['notification']['body'];
          title = message['notification']['title'];
          print(title);
          if (title == "nuevo mensaje admin") {
            print("admin");
            _colorfullAlert(argumento, () => fun("chatAdmin"));
          } else if (title == "nuevo mensaje todos") {
            print("todos");
            _colorfullAlert(argumento, () => fun("chatTodos"));
          } else if (title == "Alerta Seguridad") {
            print("alert segurity");
            _colorfullAlert(argumento, () => fun("emergencia"));
          } else if (title == "Nueva Noticia") {
            _colorfullAlert(argumento, () => fun("noticia"));
          } else if (title == "Notificación Casillero") {
            _colorfullAlert(argumento, () => fun("casillero"));
          } else if (title == "PQR Radicado") {
            _colorfullAlert(argumento, () => fun("pqr"));
          } else {
            _colorfullAlert(argumento, fun);
          }
        } else {
          argumento = message['notification']['body'];
          title = message['notification']['title'];
          print(title);
          if (title == "nuevo mensaje admin") {
            _colorfullAlert(argumento, () => fun("chatAdmin"));
          } else if (title == "nuevo mensaje todos") {
            _colorfullAlert(argumento, () => fun("chatTodos"));
          } else if (title == "Alerta Seguridad") {
            print("alert segurity");
            _colorfullAlert(argumento, () => fun("emergencia"));
          } else if (title == "Nueva Noticia") {
            _colorfullAlert(argumento, () => fun("noticia"));
          } else if (title == "Notificación Casillero") {
            _colorfullAlert(argumento, () => fun("casillero"));
          } else if (title == "PQR Radicado") {
            _colorfullAlert(argumento, () => fun("pqr"));
          } else {
            _colorfullAlert(argumento, fun);
          }
        }

        _stopSound();

        _mensajesStreamController.sink.add(argumento);
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        String title = '';

        title = message["data"]['title'];
        print(title);
        if (title == "nuevo mensaje admin") {
          print("admin");
          fun("chatAdmin");
        } else if (title == "nuevo mensaje todos") {
          print("todos");
          fun("chatTodos");
        } else if (title == "Alerta Seguridad") {
          fun("emergencia");
        } else if (title == "Nueva Noticia") {
          fun("noticia");
        } else if (title == "Notificación Casillero") {
          fun("casillero");
        } else if (title == "PQR Radicado") {
          fun("pqr");
        } else {
          fun();
        }
      },
    );
  }

  dispose() {
    _mensajesStreamController?.close();
    _firebaseMessaging.deleteInstanceID();
  }

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/Alert-notification.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _alarmSound = await _soundId;
    _alarmSoundStreamId = await _soundpool.play(_alarmSound);
  }

  Future<void> _pauseSound() async {
    if (_alarmSoundStreamId != null) {
      await _soundpool.pause(_alarmSoundStreamId);
    }
  }

  Future<void> _stopSound() async {
    if (_cheeringStreamId != null) {
      await _soundpool.stop(_cheeringStreamId);
    }
  }
}
