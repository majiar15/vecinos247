import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart' as dioClient;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/cartelera.dart';
import 'package:vecinos247/src/models/noticia.dart';
import 'package:vecinos247/src/models/pqr.dart';
import 'package:vecinos247/src/widgets/alerts.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as client;
import 'package:http/http.dart';

class NoticiasProvider {
  final String baseUrl = constantes.apiUrl;
  final String baseUrlDio = constantes.apiUrlDio;

  Future<List<Cartelera>> getAllNoticias() async {
    final Map<String, dynamic> authData = {
      "id_subunidad": appData.datosApp.lista_subunidades[0].id_subunidad
    };
    print(appData.datosApp.lista_subunidades[0].id_subunidad.toString());
    final response = await client.post("$baseUrl/unidad/noticias/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});

    final decodedData = json.decode(response.body);

    final entidades = new ListCartelera.fromJsonList(decodedData);
    print(entidades.items.reversed.toList());
    return entidades.items.reversed.toList();
  }

  Future<List<Cartelera>> getAllNoticiasAdmin(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };
    final response = await client.post("$baseUrl/admin/cartelera/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);

    List<dynamic> noticiasAdministrador = new List();
    print("======================== get All Noticias Admin");
    print(decodedData);
    // bool comprobar;
    final entidades = new ListCartelera.fromJsonList(decodedData);
    print(entidades.items.reversed.toList()[0].imagen);
    return entidades.items.reversed.toList();
    // for (int i = 0; i < decodedData.length; i++) {
    //   final response = await client.get(decodedData[i]["imagen"]);

    //   if (response.statusCode == 200) {
    //     comprobar = true;
    //   } else {
    //     comprobar = false;
    //   }

    //   var arrayNoticias = null;
    //   arrayNoticias = {
    //     "id_cartelera": decodedData[i]["id_cartelera"],
    //     "titulo": decodedData[i]["titulo"],
    //     "descripcion": decodedData[i]["descripcion"],
    //     "fecha_creacion": decodedData[i]["fecha_creacion"],
    //     "imagen": decodedData[i]["imagen"],
    //     "comprobacion": comprobar,
    //   };

    //   noticiasAdministrador.add(arrayNoticias);
    // }

    // final noticiaEncode = jsonEncode(noticiasAdministrador);
    // final noticiaDecode = jsonDecode(noticiaEncode);

    // try {

    // } catch (e) {
    //   return null;
    // }
  }

  Future<void> generarNoticia(BuildContext context, String titulo,
      String descripcion, String vencimiento, File image) async {
    // convertImage(image) {
    //   List<int> imageBytes = image.readAsBytesSync();
    //   String base64Image = base64.encode(imageBytes);
    //   print(base64Image);
    //   return base64Image;
    // }
    print("generarNoticia ===================================");
    print({
      "username": appData.datosAppSelecionado.cedula,
      "InputTitulo": titulo,
      "InputDescripcion": descripcion,
      "InputVencimiento": vencimiento,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "InputEstado": 1,
      "token": appData.datosAppSelecionado.token
    });
    dioClient.FormData authData;
    try {
      // si hay imagen no saltara error
      authData = new dioClient.FormData.fromMap({
        "username": appData.datosAppSelecionado.cedula,
        "InputTitulo": titulo,
        "InputDescripcion": descripcion,
        "InputImagen": await dioClient.MultipartFile.fromFile(image.path),
        "InputVencimiento": vencimiento,
        "InputEstado": 1,
        "token": appData.datosAppSelecionado.token
      });
    } catch (e) {
      // si hay error significa que no hay  imagen, entonces se asigna null
      authData = new dioClient.FormData.fromMap({
        "username": appData.datosAppSelecionado.cedula,
        "InputTitulo": titulo,
        "InputDescripcion": descripcion,
        "InputImagen": null,
        "InputVencimiento": vencimiento,
        "InputEstado": 1,
        "token": appData.datosAppSelecionado.token
      });
    }
    final url = Uri.https(baseUrlDio, '/api/admin/cartelera/save');
    var response = await dioClient.Dio().postUri(url,
        data: authData,
        options: dioClient.Options(headers: {
          'contentType': 'multipart/form-data',
          // set content-length
        }));
    print(response.data);
  }

  Future<Map<String, dynamic>> eliminarNoticia(BuildContext context, id) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosApp.cedula,
      "id_cartelera": id,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };
    ProgressDialog pr = new ProgressDialog(context);
    pr.style(
        message: 'Eliminando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    print(authData);
    Client client = Client();
    pr.show();
    final response = await client.post("$baseUrl/admin/cartelera/delete",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    pr.hide();
    print(response.body);
    final decodedData = json.decode(response.body);
    if (decodedData["resp"] == "ok") {
      return {"respuesta": "Noticia eliminada correctamente"};
    } else {
      return {"respuesta": "Error eliminar"};
    }
  }
}
