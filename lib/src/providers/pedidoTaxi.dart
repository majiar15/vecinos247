import 'dart:convert';

import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/pedidoTaxi.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as client;
import 'package:http/http.dart';

class PedidoTaxiProvider {
  final String baseUrl = constantes.apiUrl;

  Future<Map<String, dynamic>> pedirTaxi(
      BuildContext context, String observaciones) async {
    final Map<String, dynamic> authData = {
      "id_residente": appData.datosAppSelecionado.id,
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "observaciones": observaciones,
      "username": appData.datosAppSelecionado.cedula,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
    };

    Client client = Client();
    final response = await client.post("$baseUrl/residente/taxi/pedir",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
  }

  Future<Map<String, dynamic>> eliminarPedidoTaxi(
      BuildContext context, id) async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "id_taxi": id,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token,
    };

    Client client = Client();
    final response = await client.post("$baseUrl/residente/taxi/delete",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
    final decodedData = json.decode(response.body);
    if (decodedData["resp"] == "ok") {
      return {"respuesta": "Pedido cancelado correctamente"};
    } else {
      return {"respuesta": "Error eliminar"};
    }
  }

  Future<List<PedidoTaxi>> getAllPedidosTaxi(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "username": appData.datosAppSelecionado.cedula,
      "id_residente": appData.datosAppSelecionado.id,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token,
    };
    final response = await client.post("$baseUrl/residente/taxi/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);
    print(response.body);
    try {
      final entidades = new PedidosTaxis.fromJsonList(decodedData);
      return entidades.items.reversed.toList();
    } catch (e) {
      return null;
    }
  }
}
