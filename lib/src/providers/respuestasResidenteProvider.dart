import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/respuestaPqr.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as client;
import 'package:dio/dio.dart' as dioClient;
import 'dart:convert';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';

class RespuestaResidenteProvider {
  final String baseUrl = constantes.apiUrl;
  final String baseUrlDio = constantes.apiUrlDio;
  Future<List<RespuestaPqr>> getHilo(id, context) async {
    final Map<String, dynamic> authData = {
      "id_pqr": id,
      "id_residente": appData.datosAppSelecionado.id,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    print(authData);
    final response = await client.post("$baseUrl/residente/pqr/hilo/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    // print(response.body);
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);
    print("entiedados");
    print(decodedData);

    try {
      final entidades = new RespuestasPqr.fromJsonList(decodedData);

      return entidades.items;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<List<RespuestaPqrAdmin>> getHiloAdmin(BuildContext context, id) async {
    final Map<String, dynamic> authData = {
      "id_administrador": appData.datosAppSelecionado.id,
      "id_pqr": id,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    // print(authData);
    final response = await client.post("$baseUrl/admin/pqr/hilo/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);
    print(decodedData);
    print("=====================================");
    print("=====================================Admin Admim");
    try {
      final entidades =
          new RespuestasPqrAdmin.fromJsonList(decodedData["traza"]);

      return entidades.items;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // Future<bool> createProfile(Map<String, dynamic> data) async {
  //   final url = Uri.http(baseUrlDio, '/api/visita/save');
  //   final response = await dioClient.Dio().postUri(url,
  //       data: new dioClient.FormData.fromMap(data),
  //       options: dioClient.Options(headers: {
  //         'contentType': 'multipart/form-data',
  //         // set content-length
  //       }));
  //   print(response);
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
  Future<Map<String, dynamic>> respuestaResidente(
      descripcion, idresidente, idPqr, foto, username, context) async {
    ProgressDialog pr;

    pr = new ProgressDialog(context);
    pr.style(
        message: 'Guardando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    // convertImage(image) {
    //   List<int> imageBytes = image.readAsBytesSync();
    //   String base64Image = base64.encode(imageBytes);
    //   print(base64Image);
    //   return base64Image;
    // }
    // dioClient.FormData authData;
    //   authData = new dioClient.FormData.fromMap({
    //     "id_factura": id_factura,
    //     "adjunto": await dioClient.MultipartFile.fromFile(image.path),
    //     "id_subunidad": appData.datosAppSelecionado.id_subunidad,
    //     "token": appData.datosAppSelecionado.token,
    //     "username": appData.datosAppSelecionado.id
    //   });
    final url = Uri.https(baseUrlDio, '/api/residente/pqr/reply');
    dioClient.FormData authData;
    authData = new dioClient.FormData.fromMap({
      "descripcion": descripcion,
      "id_residente": idresidente,
      "id_pqr": idPqr,
      "foto": foto == null
          ? null
          : await dioClient.MultipartFile.fromFile(foto.path),
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    });
    // final Map<String, dynamic> data = {
    //   "descripcion": descripcion,
    //   "id_residente": idresidente,
    //   "id_pqr": idPqr,
    //   "foto": foto == null ? null : foto,
    //   "token": appData.datosAppSelecionado.token,
    //   "username": appData.datosAppSelecionado.cedula
    // };
    pr.show();
    var response = await dioClient.Dio().postUri(url,
        data: authData,
        options: dioClient.Options(headers: {
          'contentType': 'multipart/form-data',
          // set content-length
        }));
    pr.hide();

    print(authData);

    if (response.data["resp"] == "ok") {
      return {"respuesta": "Respuesta guardada correctamente"};
    } else {
      return {
        "respuesta":
            "Error al guardar, compruebe haber llenado los campos correctamente"
      };
    }
  }

  Future<Map<String, dynamic>> resolverPqrAdmin(idPqr, context) async {
    ProgressDialog pr;
    pr = new ProgressDialog(context);
    pr.style(
        message: 'Resolviendo PQR...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    final Map<String, dynamic> authData = {
      "id_pqr": idPqr,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };

    print(authData);
    pr.show();

    Client client = Client();
    final response = await client.post("$baseUrl/admin/pqr/solved",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});

    final decodedData = json.decode(response.body);
    print(response.body);
    if (decodedData["resp"] == "ok") {
      return {"respuesta": "PQR Resuelto"};
    } else {
      return {
        "respuesta":
            "Error al guardar, compruebe haber llenado los campos correctamente"
      };
    }
  }

  Future<Map<String, dynamic>> respuestaAdmin(
      descripcion, idresidente, idPqr, foto, username, context) async {
    ProgressDialog pr;
    final url = Uri.https(baseUrlDio, '/api/admin/pqr/reply');

    pr = new ProgressDialog(context);
    pr.style(
        message: 'Guardando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    // convertImage(image) {
    //   List<int> imageBytes = image.readAsBytesSync();
    //   String base64Image = base64.encode(imageBytes);
    //   print(base64Image);
    //   return base64Image;
    // }
    dioClient.FormData authData;
    authData = new dioClient.FormData.fromMap({
      "descripcion": descripcion,
      "id_administrador": idresidente,
      "id_pqr": idPqr,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "foto": foto == null
          ? null
          : await dioClient.MultipartFile.fromFile(foto.path),
      "username": appData.datosApp.cedula,
      "token": appData.datosAppSelecionado.token,
    });
    // final Map<String, dynamic> authData = {
    //   "descripcion": descripcion,
    //   "id_administrador": idresidente,
    //   "id_pqr": idPqr,
    //   "foto": foto == null ? null : convertImage(foto),
    //   "username": appData.datosApp.cedula,
    //   "token": appData.datosAppSelecionado.token,
    // };

    print(authData);
    pr.show();
    var response = await dioClient.Dio().postUri(url,
        data: authData,
        options: dioClient.Options(headers: {
          'contentType': 'multipart/form-data',
          // set content-length
        }));
    pr.hide();

    if (response.data["resp"] == "ok") {
      return {"respuesta": "Respuesta guardada correctamente"};
    } else {
      return {
        "respuesta":
            "Error al guardar, compruebe haber llenado los campos correctamente"
      };
    }
  }
}
