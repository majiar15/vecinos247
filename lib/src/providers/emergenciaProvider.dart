import 'dart:convert';

import 'package:progress_dialog/progress_dialog.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/pedidoTaxi.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as client;
import 'package:http/http.dart';

import '../constantes.dart';

class EmergenciaProvider {
  final String baseUrl = constantes.apiUrl;

  Future generarEmergencia(BuildContext context, String mensaje) async {
    final Map<String, dynamic> authData = {
      "id_residente": appData.datosAppSelecionado.id,
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "mensaje": mensaje + ' | ' + appData.datosAppSelecionado.nombre_residente,
      "username": appData.datosAppSelecionado.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token
    };
    print(appData.rol);
    print(authData);
    Client client = Client();
    final response = await client.post("$baseUrl/residente/panico/save",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});

    try {
      if (response.statusCode == 403) {
        desLogin(context);
      }
      print("response.statusCode ==============================");
      print(response.statusCode);
      print("response.statusCode");
      print(response.body);
      final decodedData = json.decode(response.body);

      return decodedData;
    } catch (a) {
      return null;
    }
  }

  Future cancelarEmergencia(BuildContext context, String mensaje, id) async {
    ProgressDialog pr = new ProgressDialog(context);
    pr.style(
        message: 'Cancelando la emergencia...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    final Map<String, dynamic> authData = {
      "id_residente": appData.datosAppSelecionado.id,
      "id_casillero": id,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "mensaje": mensaje,
      "username": appData.datosAppSelecionado.cedula,
      "token": appData.datosAppSelecionado.token
    };
    print(authData);
    Client client = Client();
    pr.show();
    final response = await client.post("$baseUrl/residente/panico/cancel",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    pr.hide();
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);

    print(decodedData);

    return decodedData;
  }
}
