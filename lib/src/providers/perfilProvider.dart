import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/perfilResidente.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as client;
import 'package:shared_preferences/shared_preferences.dart';

class PerfilProvider {
  final _url = constantes.apiUrl;

  Future<PerfilResidente> getPerfilResidente(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "cedula": appData.datosAppSelecionado.cedula,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    final prefs = await SharedPreferences.getInstance();
    final response = await client.post("$_url/residente/get",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});

    if (response.statusCode == 403) {
      desLogin(context);
      return new PerfilResidente.fromJson({});
    }
    final decodedData = json.decode(response.body);
    // print(decodedData);
    final respueta = new PerfilResidente.fromJson(decodedData);

    appData.datosApp.nombre_residente =
        respueta.inputName + " " + respueta.inputApe;
    prefs.setString('nombre', respueta.inputName);
    prefs.setString('apellido', respueta.inputApe);

    if (respueta.inputFoto != '' || respueta.inputFoto != null) {
      appData.fotoPerfil = base64.decode(respueta.inputFoto);
      String foto = new String.fromCharCodes(appData.fotoPerfil);
      prefs.setString('foto', foto);
    }

    return respueta;
  }

  Future editarPerfilResidente(
      BuildContext context, nombre, apellido, dir, tel, correo) async {
    ProgressDialog pr;
    pr = new ProgressDialog(context);
    pr.style(
        message: 'Guardando cambios...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    final Map<String, dynamic> authData = {
      "InputId": appData.datosApp.id,
      "InputCed": appData.datosApp.cedula,
      "InputName": nombre,
      "InputApe": apellido,
      "InputDir": dir,
      "InputTel": tel,
      "InputEmail": correo,
      "Estado": appData.estado,
      "InputFoto": appData.encodedFotoPerfil,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula,
    };
    print(authData);
    pr.show();
    final response = await client.post("$_url/residente/perfil/editar",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    pr.dismiss();
    if (response.statusCode == 200) {
      appData.datosAppSelecionado.nombre_residente = "$nombre $apellido";
      appData.fotoPerfil = base64.decode(appData.encodedFotoPerfil);
      appData.datosApp.nombre_residente = nombre;

      final decodedData = json.decode(response.body);
      print(decodedData);
      // print(appData.datosApp.id);
    }
  }
}
