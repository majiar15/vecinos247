import 'package:flutter/widgets.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/sub-unidad.dart';
import 'package:vecinos247/src/models/unidadmodel.dart';
import 'package:http/http.dart' as client;
import 'dart:convert';

class SubUnidadProvider {
  final String baseUrl = constantes.apiUrl;
  Future<String> setSubUnidad(DatosSubUnidad datosSubUnidad) async {
    final Map<String, dynamic> authData = {
      ...getSubUnidadLogin(datosSubUnidad),
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };

    final response = await client.post("$baseUrl/admin/subunidad/save",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
    print(authData);
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      if (datos["resp"] != "error") {
        return datos["msj"];
      } else {
        return datos["msj"];
      }
    } else {
      return "Ocurrio un error";
    }
  }

  Future<Unidad> getUnidad(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "id_residente": appData.datosAppSelecionado.id,
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };

    if (appData.datosAppSelecionado.id_subunidad == null) {
      return new Unidad();
    }

    final response = await client.post("$baseUrl/unidad/get/basico",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
      return null;
    }
    print(response.body);
    final decodedData = json.decode(response.body);
    final entidades = new Unidad.fromJson(decodedData);

    return entidades;
  }
}
