import 'dart:convert';
import 'dart:io';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/pqr.dart';
import 'package:vecinos247/src/widgets/alerts.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as client;
import 'package:dio/dio.dart' as dioClient;
import 'package:http/http.dart';

import '../constantes.dart';

class PqrProvider {
  final String baseUrl = constantes.apiUrl;
  final String baseUrlDio = constantes.apiUrlDio;

  Future<void> generarPqr(BuildContext context, String descripcion, File image,
      tipo_destino, _tipo_prq) async {
    // convertImage(image) {
    //   List<int> imageBytes = image.readAsBytesSync();
    //   String base64Image = base64.encode(imageBytes);
    //   print(base64Image);
    //   return base64Image;
    // }

    print(appData.dirigido_a);
    print(appData.tipo_pqr);
    // final Map<String, dynamic> authData = {
    //   "id_residente": appData.datosAppSelecionado.id,
    //   "id_subunidad": appData.datosAppSelecionado.id_subunidad,
    //   "descripcion": descripcion,
    //   "dirigido_a": tipo_destino,
    //   "id_tipo_pqr": _tipo_prq,
    //   "username": appData.datosAppSelecionado.cedula,
    //   "foto": image != null ? convertImage(image) : '',
    //   "token": appData.datosAppSelecionado.token,
    // };
    final url = Uri.https(baseUrlDio, '/api/residente/pqr/save');
    dioClient.FormData authData;
    authData = new dioClient.FormData.fromMap({
      "id_residente": appData.datosAppSelecionado.id,
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "descripcion": descripcion,
      "dirigido_a": tipo_destino,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "id_tipo_pqr": _tipo_prq,
      "username": appData.datosAppSelecionado.cedula,
      "token": appData.datosAppSelecionado.token,
      "foto": image == null
          ? null
          : await dioClient.MultipartFile.fromFile(image.path),
    });
    print(authData);
    var response = await dioClient.Dio().postUri(url,
        data: authData,
        options: dioClient.Options(headers: {
          'contentType': 'multipart/form-data',
          // set content-length
        }));
    // Client client = Client();
    // final response = await client.post("$baseUrl/residente/pqr/save",
    //     body: json.encode(authData),
    //     headers: {"Content-Type": "application/json"});

    print(response.data["resp"]);
  }

  Future<List<Pqr>> getAllPqrs(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "id_residente": appData.datosApp.id,
      "id_subunidad": appData.datosAppSelecionado.id_subunidad,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    final response = await client.post("$baseUrl/residente/pqr/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    if (response.statusCode == 403) {
      desLogin(context);
    }
    final decodedData = json.decode(response.body);

    print(decodedData);
    try {
      final entidades = new Pqrs.fromJsonList(decodedData);
      return entidades.items;
    } catch (e) {
      return null;
    }
  }

  Future<List<Pqr>> getAllPqrsAdmin() async {
    final Map<String, dynamic> authData = {
      "username": appData.datosAppSelecionado.cedula,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token,
    };
    final response = await client.post("$baseUrl/admin/pqr/list",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
    try {
      final decodedData = json.decode(response.body);

      final entidades = new Pqrs.fromJsonList(decodedData);

      return entidades.items;
    } catch (e) {
      return null;
    }
  }
}
