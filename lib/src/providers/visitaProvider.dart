import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/helpers/desLogin.dart';
import 'package:vecinos247/src/models/visita.dart';
import 'package:http/http.dart' as client;

class VisitaProvider {
  final String baseUrl = constantes.apiUrl;

  Future<List<DatosVisita>> getlistavisitas(BuildContext context) async {
    final Map<String, dynamic> authData = {
      "id_residente": appData.datosAppSelecionado.id,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    try {
      final response = await client.post("$baseUrl/visita/list",
          body: json.encode(authData),
          headers: {"Content-Type": "application/json"});
      if (response.statusCode == 403) {
        desLogin(context);
      }
      print(response.body.toString());
      final decodedData = json.decode(response.body);
      final entidades = new ListaVisitante.fromJsonList(decodedData);
      return entidades.items.reversed.toList();
    } catch (a) {
      return [];
    }
  }
}
