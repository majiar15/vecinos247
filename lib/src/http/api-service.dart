import 'package:vecinos247/clases/datos_sesion.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/http/datos-login.dart';
import 'package:vecinos247/src/http/datos-visitas.dart';
import 'package:vecinos247/src/models/appdata_model.dart';
import 'package:vecinos247/src/pages/admin/drawer_admin.dart';
import 'package:vecinos247/src/pages/login/afterLogin.dart';
import 'package:vecinos247/src/pages/residente/drawer.dart';
import 'package:vecinos247/src/providers/perfilProvider.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:dio/dio.dart' as dioClient;
import 'package:progress_dialog/progress_dialog.dart';

import '../helpers/appdata.dart';
import 'datos-reserva.dart';

class ApiService {
  final perfilProvider = PerfilProvider();

  final String baseUrl = constantes.apiUrl;
  final String baseUrlDio = constantes.apiUrlDio;
  Client client = Client();

  Future<dynamic> borrarReserva(int id, BuildContext context) async {
    ProgressDialog pr = new ProgressDialog(context);
    pr.style(
        message: 'Eliminando reserva...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));
    pr.show();
    final response = await client.post(
      "$baseUrl/reserva/delete",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "id_reserva": id,
        "token": appData.datosAppSelecionado.token,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Admin"
                ? 300
                : 200,
        "username": appData.datosAppSelecionado.cedula,
      }),
    );
    pr.hide();
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      try {
        if (datos['resp']) {
          return null;
        }
      } catch (Exception) {
        return datos;
      }
    } else {
      print('4');
      return null;
    }
  }

  Future<dynamic> InformacionZona() async {
    final response = await client.post(
      "$baseUrl/zonas/disponibilidad/diasemana/all",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "token": appData.datosAppSelecionado.token,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Admin"
                ? 300
                : 200,
        "username": appData.datosAppSelecionado.cedula
      }),
    );
    if (response.statusCode == 200) {
      final datos = jsonDecode(response.body);
      print(datos);
      try {
        return datos;
      } catch (Exception) {
        return "error";
      }
    } else {
      print('4');
      return null;
    }
  }

  Future<dynamic> borrarFamiliar(int id) async {
    final response = await client.post(
      "$baseUrl/nucleo_familiar/delete",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "id_nucleo": id,
        "token": appData.datosAppSelecionado.token,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Admin"
                ? 300
                : 200,
        "username": appData.datosAppSelecionado.cedula
      }),
    );
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      try {
        if (datos['resp']) {
          return null;
        }
      } catch (Exception) {
        return datos;
      }
    } else {
      print('4');
      return null;
    }
  }

  Future<dynamic> solicitarlistaOtros(String id) async {
    final response = await client.post(
      "$baseUrl/nucleo_familiar/list/otros",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "cedula": id,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Administrador"
                ? 300
                : 200,
      }),
    );
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      print(datos);
      try {
        if (datos['resp']) {
          return null;
        }
      } catch (Exception) {
        return datos;
      }
    } else {
      print('4');
      return null;
    }
  }

  Future<dynamic> solicitarlistaHijos(String id) async {
    final response = await client.post(
      "$baseUrl/nucleo_familiar/list/hijos",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "cedula": id,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Administrador"
                ? 300
                : 200,
      }),
    );
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      print(datos);
      try {
        if (datos['resp']) {
          return null;
        }
      } catch (Exception) {
        return datos;
      }
    } else {
      print('4');
      return null;
    }
  }

  Future<dynamic> solicitarConyuge(String id) async {
    final response = await client.post(
      "$baseUrl/nucleo_familiar/get/conyuge",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "cedula": id,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Administrador"
                ? 300
                : 200,
      }),
    );
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      print(datos);
      try {
        if (datos['resp']) {
          return null;
        }
      } catch (Exception) {
        return datos;
      }
    } else {
      print('4');
      return null;
    }
  }

  Future<List<dynamic>> listarVsitas(String id) async {
    final response = await client.post(
      "$baseUrl/visita/list",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "id_residente": id,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Administrador"
                ? 300
                : 200,
      }),
    );
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      try {
        if (datos['resp'] == "error") {
          return null;
        }
      } catch (Exeption) {
        return datos;
      }
    } else {
      return null;
    }
  }

  Future<List<dynamic>> listarZona() async {
    final Map<String, dynamic> authData = {
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };
    final response = await client.post("$baseUrl/zonas/list",
        headers: {"content-type": "application/json"},
        body: json.encode(authData));
    print(response.body);
    final datos = json.decode(response.body);
    try {
      return datos;
    } catch (e) {
      return null;
    }
  }

  Future<dynamic> getDisponibilidadZonaSemana(
    String id_zona,
  ) async {
    final Map<String, dynamic> authData = {
      "id_zona": id_zona,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "username": appData.datosAppSelecionado.cedula
    };

    final response = await client.post(
        "$baseUrl/zonas/disponibilidad/diasemana",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      try {
        if (datos['resp'] == "error") {
          return datos['resp'];
        }
      } catch (Exeption) {
        return datos;
      }
    } else {
      return null;
    }
    return json.decode(response.body);
  }

  Future<dynamic> getDisponibilidadZona(String id_zona, String fecha) async {
    final Map<String, dynamic> authData = {
      "id_zona": id_zona,
      "fecha": fecha,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "token": appData.datosAppSelecionado.token,
      "username": appData.datosAppSelecionado.cedula
    };

    final response = await client.post("$baseUrl/zonas/disponibilidad",
        body: json.encode(authData),
        headers: {"Content-Type": "application/json"});
    print(response.body);
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      try {
        if (datos['resp'] == "error") {
          return datos['resp'];
        }
      } catch (Exeption) {
        return datos;
      }
    } else {
      return null;
    }
    return json.decode(response.body);
  }

  Future<List<dynamic>> listarReserva(String id) async {
    final response = await client.post(
      "$baseUrl/reserva/list",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "id_subunidad": id,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Administrador"
                ? 300
                : 200,
      }),
    );
    print(response.body);
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      try {
        if (datos['resp'] == "error") {
          return null;
        }
      } catch (Exeption) {
        return datos;
      }
    } else {
      return null;
    }
  }

  Future<String> guardarReserva(DatosReserva data) async {
    data.token = appData.datosAppSelecionado.token;
    final response = await client.post(
      "$baseUrl/reserva/save",
      headers: {"content-type": "application/json"},
      body: reservaToJson(data),
    );

    print(response.body);
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      if (datos["resp"] == "ok") {
        return 'ok';
      } else {
        return datos["msj"];
      }
    } else {
      return 'Ocurrió un error';
    }
  }

  Future<String> guardarFamiliar(
      id_nucleo,
      id_parentesco,
      id_residente,
      id_familiar,
      cedula_familiar,
      nombre_familiar,
      correo_familiar,
      movil_familiar,
      direccion_familiar,
      tipo_identificacion,
      token) async {
    final response = await client.post(
      "$baseUrl/nucleo_familiar/save",
      headers: {"content-type": "application/json"},
      body: json.encode({
        "id_nucleo": id_nucleo,
        "id_parentesco": id_parentesco,
        "id_residente": id_residente,
        "id_familiar": id_familiar,
        "cedula_familiar": cedula_familiar,
        "nombre_familiar": nombre_familiar,
        "apellidos_familiar": "",
        "correo_familiar": correo_familiar,
        "movil_familiar": movil_familiar,
        "direccion_familiar": direccion_familiar,
        "username": appData.datosApp.cedula,
        "tipo_documento": tipo_identificacion,
        "token": appData.datosAppSelecionado.token,
        "id_perfil": appData.rol == "Residente"
            ? 100
            : appData.rol == "Admin"
                ? 300
                : 200,
        "id_unidad": appData.datosAppSelecionado.id_unidad,
        "estado": 1
      }),
    );
    print(json.encode({
      "id_nucleo": id_nucleo,
      "id_parentesco": id_parentesco,
      "id_residente": id_residente,
      "id_familiar": id_familiar,
      "cedula_familiar": cedula_familiar,
      "nombre_familiar": nombre_familiar,
      "apellidos_familiar": "",
      "correo_familiar": correo_familiar,
      "movil_familiar": movil_familiar,
      "direccion_familiar": direccion_familiar,
      "username": appData.datosApp.cedula,
      "tipo_documento": tipo_identificacion,
      "token": appData.datosAppSelecionado.token,
      "id_perfil": appData.rol == "Residente"
          ? 100
          : appData.rol == "Admin"
              ? 300
              : 200,
      "id_unidad": appData.datosAppSelecionado.id_unidad,
      "estado": 1
    }));
    print(response.body);
    if (response.statusCode == 200) {
      final datos = json.decode(response.body);
      if (datos["msj"] == "ok") {
        return "";
      } else {
        return datos["msj"];
      }
    } else {
      return "Ocurrió un error";
    }
  }

  Future<bool> createProfile(Map<String, dynamic> data) async {
    final url = Uri.https(baseUrlDio, '/api/visita/save');
    print(data);
    final response = await dioClient.Dio().postUri(url,
        data: new dioClient.FormData.fromMap({
          ...data,
          "id_perfil": appData.rol == "Residente"
              ? 100
              : appData.rol == "Admin"
                  ? 300
                  : 200,
        }),
        options: dioClient.Options(headers: {
          'contentType': 'multipart/form-data',
          // set content-length
        }));
    print(response);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<dynamic> login(DatosLogin datosLogin, ProgressDialog pr, context,
      usuario, password, estado) async {
    final prefs = await SharedPreferences.getInstance();
    try {
      pr.show();
      final response = await client.post(
        "$baseUrl/usuario/login",
        headers: {"content-type": "application/json"},
        body: profileToJsonLogin(datosLogin),
      );
      if (response.statusCode == 403) {
        return "inactivo";
      } else if (response.statusCode == 200) {
        print("=========================================");
        print(response.body.toString());
        print("========================================= datos");
        final datos = json.decode(response.body);
        DatosApp datosApp = DatosApp.fromJson(datos);
        print(datos);
        if (datosApp.lista_subunidades.length == 0) {
          pr.dismiss();
          return "inactivo";
        } else if (datosApp.resp == "ok") {
          prefs.setString("Informacion", response.body);

          DatosSesion datosSesion = DatosSesion();
          datosSesion.sesionActiva(datosApp);
          print("=========================================== login add");
          if (appData.datosAppSelecionado.perfiles.length == 1) {
            if (appData.datosApp.lista_subunidades[0].perfiles[0].id_perfil ==
                100) {
              appData.datosAppSelecionado.permiso = "100";
              appData.rol = "Residente";
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                if (appData.rol == "Residente" || appData.rol == "Familiar") {
                  return DrawerItem();
                } else {
                  return DrawerAdminItem();
                }
              }));
            } else if (appData
                    .datosApp.lista_subunidades[0].perfiles[0].id_perfil ==
                300) {
              appData.datosAppSelecionado.permiso = "300";
              appData.rol = "Admin";
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                if (appData.rol == "Residente" || appData.rol == "Familiar") {
                  return DrawerItem();
                } else {
                  return DrawerAdminItem();
                }
              }));
            } else {
              appData.datosAppSelecionado.permiso = "200";
              appData.rol = "Familiar";
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                if (appData.rol == "Residente" || appData.rol == "Familiar") {
                  return DrawerItem();
                } else {
                  return DrawerAdminItem();
                }
              }));
            }
          } else {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => AfterLoginPage()));
          }
          return true;
        } else {
          pr.dismiss();
          return false;
        }
      } else {
        pr.dismiss();
        return false;
      }
    } catch (e) {
      print("error de cuenta");
      print(e);
      pr.dismiss();
      //prefs.clear();
      return false;
    }
  }
}
