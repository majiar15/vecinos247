class DisponibilidadReserva {
  int id_zona;
  String nombre_zona;
  List<dynamic> disponibilidades;

  DisponibilidadReserva(
      {this.id_zona, this.disponibilidades, this.nombre_zona});
  factory DisponibilidadReserva.fromJson(Map<String, dynamic> json) =>
      DisponibilidadReserva(
        id_zona: json['id_zona'],
        disponibilidades: json["disponibilidades"],
        nombre_zona: json['nombre_zona'],
      );
}

class ListaDisponibilidadReserva {
  List<DisponibilidadReserva> items = new List();
  ListaDisponibilidadReserva();
  ListaDisponibilidadReserva.fromJsonList(List<dynamic> jsonlist) {
    if (jsonlist == null) return;
    for (var item in jsonlist) {
      final horas = new DisponibilidadReserva.fromJson(item);
      items.add(horas);
    }
  }
}

class DatosHoraDisponible {
  String horas_disponibles;

  DatosHoraDisponible({this.horas_disponibles});
  factory DatosHoraDisponible.fromJson(Map<String, dynamic> json) =>
      DatosHoraDisponible(
        horas_disponibles: json['horas_disponibles'],
      );
}

class ListaHoraDisponible {
  List<DatosHoraDisponible> items = new List();
  ListaHoraDisponible();
  ListaHoraDisponible.fromJsonList(List<dynamic> jsonlist) {
    if (jsonlist == null) return;
    for (var item in jsonlist) {
      final horas = new DatosHoraDisponible.fromJson(item);
      items.add(horas);
    }
  }
}
