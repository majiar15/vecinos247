import 'dart:convert';

Emergencia EmergenciaFromJson(String str) =>
    Emergencia.fromJson(json.decode(str));

String EmergenciaToJson(Emergencia data) => json.encode(data.toJson());

class Emergencias {
  List<Emergencia> items = new List();

  Emergencias();

  Emergencias.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      if (item["id_tipo_casillero"] == 1) {
        print('taxi');
      } else {
        final reserva = new Emergencia.fromJson(item);
        items.add(reserva);
      }
    }
  }

  Emergencias.fromJsonListNotificaciones(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final reserva = new Emergencia.fromJson(item);
      items.add(reserva);
    }
  }

  Emergencias.fromJsonListNotificacionesEnviadas(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      if (item["hilo"] != 0) {
        final reserva = new Emergencia.fromJson(item);
        items.add(reserva);
      }
    }
  }
}

class Emergencia {
  int idCasillero;
  dynamic titulo;
  String descripcion;
  String residente;
  String nomSubunidad;
  int idTipoCasillero;
  int estado;
  String fechaCreacion;
  int hilo;

  Emergencia(
      {this.idCasillero,
      this.titulo,
      this.descripcion,
      this.nomSubunidad,
      this.residente,
      this.idTipoCasillero,
      this.estado,
      this.fechaCreacion,
      this.hilo});

  factory Emergencia.fromJson(Map<String, dynamic> json) => Emergencia(
      idCasillero: json["id_casillero"],
      titulo: json["titulo"],
      residente: json["nom_residente"],
      descripcion: json["descripcion"],
      nomSubunidad: json["nom_subunidad"],
      idTipoCasillero: json["id_tipo_casillero"],
      estado: json["estado"],
      fechaCreacion: json["fecha_creacion"],
      hilo: json["hilo"]);

  Map<String, dynamic> toJson() => {
        "id_casillero": idCasillero,
        "titulo": titulo,
        "descripcion": descripcion,
        "id_tipo_casillero": idTipoCasillero,
        "estado": estado,
        "fecha_creacion": fechaCreacion,
        "hilo": hilo
      };
}
