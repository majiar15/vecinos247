// To parse this JSON data, do
//
//     final respuestaPqr = respuestaPqrFromJson(jsonString);

import 'dart:convert';

List<RespuestaPqr> respuestaPqrFromJson(String str) => List<RespuestaPqr>.from(
    json.decode(str).map((x) => RespuestaPqr.fromJson(x)));

String respuestaPqrToJson(List<RespuestaPqr> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RespuestasPqr {
  List<RespuestaPqr> items = new List();

  RespuestasPqr();
  RespuestasPqr.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final reserva = new RespuestaPqr.fromJson(item);
      items.add(reserva);
    }
  }
}

class RespuestaPqr {
  String residente;
  dynamic administrador;
  String mensaje;
  int estado;
  String fechaCreacion;
  String foto;
  String fotoWeb;
  int idTipoPqr;
  String destinatario;

  RespuestaPqr({
    this.residente,
    this.administrador,
    this.mensaje,
    this.estado,
    this.fechaCreacion,
    this.foto,
    this.fotoWeb,
    this.idTipoPqr,
    this.destinatario,
  });

  factory RespuestaPqr.fromJson(Map<String, dynamic> json) => RespuestaPqr(
        residente: json["residente"],
        administrador: json["administrador"],
        mensaje: json["mensaje"],
        estado: json["estado"],
        fechaCreacion: json["fecha_creacion"],
        foto: json["foto"],
        fotoWeb: json["foto_web"],
        idTipoPqr: json["id_tipo_pqr"],
        destinatario: json["destinatario"],
      );

  Map<String, dynamic> toJson() => {
        "residente": residente,
        "administrador": administrador,
        "mensaje": mensaje,
        "estado": estado,
        "fecha_creacion": fechaCreacion,
        "foto": foto,
        "foto_web": fotoWeb,
        "id_tipo_pqr": idTipoPqr,
        "destinatario": destinatario,
      };
}

class RespuestasPqrAdmin {
  List<RespuestaPqrAdmin> items = new List();

  RespuestasPqrAdmin();
  RespuestasPqrAdmin.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final reserva = new RespuestaPqrAdmin.fromJson(item);
      items.add(reserva);
    }
  }
}

class RespuestaPqrAdmin {
  String mensaje;
  String fechaCreacion;
  String foto;

  RespuestaPqrAdmin({
    this.mensaje,
    this.fechaCreacion,
    this.foto,
  });

  factory RespuestaPqrAdmin.fromJson(Map<String, dynamic> json) =>
      RespuestaPqrAdmin(
        mensaje: json["mensaje"],
        fechaCreacion: json["fecha_creacion"],
        foto: json["foto"],
      );

  Map<String, dynamic> toJson() => {
        "mensaje": mensaje,
        "fecha_creacion": fechaCreacion,
        "foto": foto,
      };
}
