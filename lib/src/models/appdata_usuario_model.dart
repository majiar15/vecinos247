import 'appdata_model.dart';

class DatosAppSelecionado {

  int id;
  int cedula;
  String nombre_residente;
  int id_unidad;
  String nombre_unidad;
  String url_pago;
  int id_subunidad;
  String nombre_subunidad;
  String nombre_tipo_unidad;
  int saldo;
  String token;
  String permiso;
  List<DatosAppPerfiles> perfiles;

  DatosAppSelecionado(
      {this.cedula,
      this.id,
      this.id_subunidad,
      this.id_unidad,
      this.nombre_residente,
      this.nombre_subunidad,
      this.nombre_tipo_unidad,
      this.nombre_unidad,
      this.saldo,
      this.token,
      this.url_pago,
      this.permiso,
      this.perfiles});

  factory DatosAppSelecionado.fromJson(Map<String, dynamic> json) {
    return DatosAppSelecionado(
          cedula: json['cedula'],
          id: json['id'],
          id_subunidad: json['id_subunidad'],
          id_unidad: json['id_unidad'],
          nombre_residente: json['nombre_residente'],
          nombre_subunidad: json['nombre_subunidad'],
          nombre_tipo_unidad: json['nombre_tipo_unidad'],
          nombre_unidad: json['nombre_unidad'],
          saldo: json['saldo'],
          token: json['token'],
          url_pago: json['url_pago'],
          permiso: json['permiso'],
          perfiles: json['perfiles']
      );
  }
      
}
