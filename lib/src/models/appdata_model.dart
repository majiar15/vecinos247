class DatosApp {
  String resp;
  String msj;
  int id;
  int cedula;
  String nombre_residente;
  List<DatosAppSubUnidades> lista_subunidades;

  DatosApp(
      {this.cedula,
      this.id,
      this.lista_subunidades,
      this.msj,
      this.nombre_residente,
      this.resp});

  factory DatosApp.fromJson(Map<String, dynamic> json) => DatosApp(
      id: json['id'],
      cedula: json['cedula'],
      lista_subunidades: (json['lista_subunidades'] as List)
          ?.map((e) => e == null
              ? null
              : DatosAppSubUnidades.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      msj: json['msj'],
      nombre_residente: json['nombre_residente'],
      resp: json['resp']);
}

class DatosAppSubUnidades {
  int id_unidad;
  String nombre_unidad;
  String url_pago;
  int id_subunidad;
  String nombre_subunidad;
  String nombre_tipo_unidad;
  int saldo;
  String token;
  List<DatosAppPerfiles> perfiles;
  DatosAppSubUnidades(
      {this.id_subunidad,
      this.id_unidad,
      this.nombre_subunidad,
      this.nombre_tipo_unidad,
      this.nombre_unidad,
      this.perfiles,
      this.saldo,
      this.token,
      this.url_pago});
  factory DatosAppSubUnidades.fromJson(Map<String, dynamic> json) =>
      DatosAppSubUnidades(
          id_subunidad: json['id_subunidad'],
          id_unidad: json['id_unidad'],
          nombre_subunidad: json['nombre_subunidad'],
          nombre_tipo_unidad: json['nombre_tipo_unidad'],
          nombre_unidad: json['nombre_unidad'],
          perfiles: (json['perfiles'] as List)
              ?.map((e) => e == null
                  ? null
                  : DatosAppPerfiles.fromJson(e as Map<String, dynamic>))
              ?.toList(),
          saldo: json['saldo'],
          token: json['token'],
          url_pago: json['url_pago']);
}

class DatosAppPerfiles {
  int id_perfil;
  DatosAppPerfiles({this.id_perfil});

  factory DatosAppPerfiles.fromJson(Map<String, dynamic> json) =>
      DatosAppPerfiles(id_perfil: json['id_perfil']);
}
