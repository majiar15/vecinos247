import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:http/http.dart' as client;

import '../constantes.dart';

Isolate _isolate;
final String baseUrl = constantes.apiUrl;

Future<ReceivePort> ComprobarNoticias(noticia) async {
  ReceivePort receivePort = new ReceivePort();
  _isolate = await Isolate.spawn(
      obtenerTodo, [
    receivePort.sendPort,
    noticia,
  ]);
  return receivePort;
}

stopActualizarServicio() {
  if(_isolate != null){
    _isolate.kill(priority: Isolate.immediate);
  }
}



Future<void> obtenerTodo(List params) async {

  SendPort sendPort = params[0];
  var noticia = params[1];
  var noticiaEncode = jsonEncode(noticia);
  var noticiaDecode = jsonDecode(noticiaEncode);

  List<dynamic> noticiasAdministrador = new List();

  bool comprobar;
  for(int i = 0; i<noticiaDecode.length; i++) {

    final response = await client.get(noticiaDecode[i]["imagen"]);

    if(response.statusCode == 200){
      comprobar = true;
    }else{
      comprobar = false;
    }

    var arrayNoticias = null;
    arrayNoticias = {
      "id_cartelera" : noticiaDecode[i]["id_cartelera"],
      "titulo" : noticiaDecode[i]["titulo"],
      "descripcion" : noticiaDecode[i]["descripcion"],
      "fecha_creacion" : noticiaDecode[i]["fecha_creacion"],
      "imagen" : noticiaDecode[i]["imagen"],
      "comprobacion" : comprobar,
    };

    noticiasAdministrador.add(arrayNoticias);

  }

  sendPort.send(jsonEncode(noticiasAdministrador));

}