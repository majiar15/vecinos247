import 'dart:convert';
import 'dart:io';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/models/respuestaPqr.dart';
import 'package:vecinos247/src/pages/admin/drawer_admin.dart';
import 'package:vecinos247/src/pages/residente/drawer.dart';
import 'package:vecinos247/src/providers/noticiasProvider.dart';
import 'package:vecinos247/src/providers/pqrProvider.dart';
import 'package:vecinos247/src/providers/respuestasResidenteProvider.dart';
import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/widgets/alerts.dart';
import 'package:vecinos247/src/widgets/cardRespuestaPqr.dart';
import 'package:vecinos247/src/widgets/responderPqrAlert.dart';
import 'package:vecinos247/src/widgets/respuestaPqrAdminAlert.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'heroPhotoView.dart';

verNoticia(BuildContext context, titulo, descripcion, imagen, creacion, id,
    comprobacion, funcion) {
  final String baseUrl = constantes.apiUrl;
  final respuestapqrProvider = RespuestaResidenteProvider();
  ProgressDialog pr;
  final noticiaProvider = NoticiasProvider();
  pr = new ProgressDialog(context);
  pr.style(
      message: 'Guardando...',
      borderRadius: 10.0,
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
      ),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      progress: 0.0,
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
          color: Colors.black, fontSize: 10.0, fontWeight: FontWeight.w400),
      messageTextStyle: TextStyle(
          color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

  pr = new ProgressDialog(context);
  Future alertClose(BuildContext context, texto, rol) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 50.0,
                  child: Image(
                    image: Image.asset('recursos/logo/logo.png').image,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(texto),
              ],
            ),
            actions: <Widget>[
              rol != null
                  ? FlatButton(
                      child: Text(
                        'Cancelar',
                        style:
                            TextStyle(color: Color.fromRGBO(205, 105, 55, 1.0)),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },

                      /* onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ListaPeticionesReservaPage() ),
              )*/
                    )
                  : null,
              FlatButton(
                child: Text(
                  'Aceptar',
                  style: TextStyle(color: Color.fromRGBO(205, 105, 55, 1.0)),
                ),
                onPressed: () {
                  rol != null
                      ? Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  rol == "Residente" || rol == "Familiar"
                                      ? DrawerItem()
                                      : DrawerAdminItem()))
                      : Navigator.pop(context);
                },

                /* onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ListaPeticionesReservaPage() ),
              )*/
              ),
            ],
          );
        });
  }

  final String urlImage = constantes.urlImage;
  return showDialog(
    context: context,
    builder: (context) {
      final observaciones = TextEditingController();
      File _image;
      // add StatefulBuilder to return value

      return StatefulBuilder(
        builder: (context, setState) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(0.0),
            backgroundColor: Colors.transparent,
            content: SingleChildScrollView(
              child: Container(
                width: 400,
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.orangeAccent.shade400,
                    width: 4,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 3,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: Icon(Icons.close),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Container(
                    height: 40.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.grey.shade200,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(creacion,
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey.shade700,
                              fontFamily: 'CenturyGothic',
                              fontWeight: FontWeight.bold,
                            )),
                      ],
                    ),
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.0),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 40.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.grey.shade200,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(titulo,
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey.shade700,
                              fontFamily: 'CenturyGothic',
                              fontWeight: FontWeight.bold,
                            )),
                      ],
                    ),
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.0),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  ConstrainedBox(
                    constraints: new BoxConstraints(
                      maxHeight: 80.0,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3.0),
                        color: Colors.grey.shade200,
                      ),
                      padding: EdgeInsets.all(10.0),
                      child: Scrollbar(
                        child: new ListView(
                          shrinkWrap: true,
                          children: <Widget>[
                            Container(
                              height: 200.0,
                              child: Text(descripcion,
                                  maxLines: null,
                                  style: TextStyle(
                                    fontSize: 13.0,
                                    color: Colors.grey.shade700,
                                    fontFamily: 'CenturyGothic',
                                    fontWeight: FontWeight.bold,
                                  )),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            width: 80.0,
                            height: 80.0,
                            color: Colors.grey.shade200,
                            child: imagen == "/storage/"
                                ? Center(
                                    child: new Image.asset(
                                        'recursos/imagenes/Noticia.png'),
                                  )
                                : GestureDetector(
                                    onTap: () => {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  HeroPhotoViewRouteWrapper(
                                                    imageProvider: NetworkImage(
                                                        urlImage + imagen),
                                                  )))
                                    },
                                    child: Center(
                                        child: Image(
                                      image: NetworkImage(urlImage + imagen),
                                    )),
                                  )
                            //Image.memory(base64.decode(imagen))
                            ),
                      ],
                    ),
                  ),
                  appData.rol != "Residente"
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () async {
                                var response = await noticiaProvider
                                    .eliminarNoticia(context, id);
                                print(response);

                                Navigator.pop(context);
                                setState(() {});
                                funcion();
                                await alertClose(
                                    context, response["respuesta"], null);
                                Navigator.pop(context);
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 10.0),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 5.0),
                                child: Center(
                                  child: Text(
                                    'ELIMINAR',
                                    style: TextStyle(
                                        color: Colors.orange.shade700,
                                        fontFamily: 'CenturyGothic',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17.0),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.grey.shade300,
                                    borderRadius: BorderRadius.circular(5.0)),
                              ),
                            ),
                          ],
                        )
                      : Container()
                ]),
              ),
            ),
          );
        },
      );
    },
  );
}
