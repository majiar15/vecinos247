import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Recuperar contraseña"),
        backgroundColor: Colors.orange.shade800,
      ),
      body: WebView(
        initialUrl: 'https:/app.klegus.com/user/password/recuperar',
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
