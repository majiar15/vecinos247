import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/models/appdata_model.dart';
import 'package:vecinos247/src/widgets/alerts.dart';
import 'package:flutter/material.dart';

class AfterLoginPage extends StatefulWidget {
  const AfterLoginPage({Key key, DatosApp datosApp}) : super(key: key);

  @override
  _AfterLoginPageState createState() => _AfterLoginPageState();
}

class _AfterLoginPageState extends State<AfterLoginPage> {
  var color1 = Colors.transparent;
  var color2 = Colors.transparent;
  var seleccionado = 'Ninguno';

  int permission;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final logo = Container(
      height: size.height * 0.33,
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        child: Image(
          image: Image.asset("recursos/logo/logo-white.png").image,
        ),
      ),
    );

    final List<Widget> buttons = [];
    final Map<int, Map<String, dynamic>> rols = {
      100: {
        "color": color1,
        "name": "Residente",
        "permission": "100",
        "url": "recursos/imagenes/residenteicon.png"
      },
      200: {
        "color": color1,
        "name": "Familiar",
        "permission": "200",
        "url": "recursos/imagenes/residenteicon.png"
      },
      300: {
        "color": color2,
        "name": "Administrador",
        "permission": "300",
        "url": "recursos/imagenes/adminicon.png"
      },
    };
    print("Este es su id " +
        appData.datosAppSelecionado.perfiles[0].id_perfil.toString());
    for (var rol in appData.datosAppSelecionado.perfiles) {
      permission = rol.id_perfil;
      buttons.add(getRol(rols[permission]));
    }
    print(buttons);

    final botones = Column(
      children: <Widget>[
        SizedBox(
          height: 260.0,
          child: ListView.builder(
            reverse: true,
            itemCount: appData.datosAppSelecionado.perfiles.length,
            itemBuilder: (BuildContext context, int index) {
              // print(appData.datosAppSelecionado.perfiles[index].id_perfil);

              return _tipoUsuario(appData
                  .datosAppSelecionado
                  .perfiles[
                      appData.datosAppSelecionado.perfiles.length - 1 - index]
                  .id_perfil);
            },
          ),
        ),
      ],
    );

    final leyenda = Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'SELECCIONA TU ROL',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white,
              fontFamily: 'CenturyGothic',
              fontWeight: FontWeight.bold,
              fontSize: 25.0,
              letterSpacing: 1.2),
        ),
      ],
    );

    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                //Color.fromRGBO(205, 105, 55,1.0),
                Color.fromRGBO(168, 86, 0, 1.0),
                Color.fromRGBO(211, 94, 0, 1.0),

                Color.fromRGBO(255, 114, 0, 1.0),

                Color.fromRGBO(255, 135, 5, 1.0),
              ])),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Center(
                    child: ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    logo,
                    SizedBox(height: 30.0),
                    leyenda,
                    SizedBox(height: 10.0),
                    botones,
                    SizedBox(height: 20.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30.0),
                      child: Container(
                        height: 6.0,
                        width: 300.0,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                    SizedBox(height: 50.0),
                    SizedBox(
                      height: 20.0,
                    ),
                  ],
                )),
              ),
              Container(
                height: size.width * 0.1,
                child: Image.asset('recursos/imagenes/LogoEmpresa.png'),
              )
            ],
          ),
        ));
  }

  Widget _tipoUsuario(perfil) {
    if (perfil == 100) {
      return Column(
        children: <Widget>[
          FlatButton(
              color: color1,
              onPressed: () {
                appData.datosAppSelecionado.permiso = "100";
                appData.rol = "Residente";
                GenericAlert(
                    context,
                    "¿Está seguro de que desea entrar con el rol de " +
                        appData.rol +
                        "?",
                    appData.rol);
              },
              child: Column(
                children: <Widget>[
                  Container(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 41.0,
                      child: Image.asset('recursos/imagenes/residenteicon.png'),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Text(
                    'RESIDENTE',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'CenturyGothic',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.2),
                  ),
                  SizedBox(height: 15.0),
                ],
              ))
        ],
      );
    } else if (perfil == 300) {
      return Column(
        children: <Widget>[
          FlatButton(
              color: color2,
              onPressed: () {
                appData.datosAppSelecionado.permiso = "300";
                appData.rol = "Admin";
                GenericAlert(
                    context,
                    "¿Está seguro de que desea entrar con el rol de " +
                        appData.rol +
                        "?",
                    appData.rol);
              },
              child: Column(
                children: <Widget>[
                  Container(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 40.0,
                      child: Image.asset('recursos/imagenes/adminicon.png'),
                    ),
                  ),
                  SizedBox(height: 8.0),
                  Text(
                    'ADMIN',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'CenturyGothic',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.2),
                  ),
                  SizedBox(height: 15.0),
                ],
              ))
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          FlatButton(
              color: color1,
              onPressed: () {
                appData.datosAppSelecionado.permiso = "200";
                appData.rol = "Familiar";
                GenericAlert(
                    context,
                    "¿Está seguro de que desea entrar con el rol de " +
                        appData.rol +
                        "?",
                    appData.rol);
              },
              child: Column(
                children: <Widget>[
                  Container(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 41.0,
                      child: Image.asset('recursos/imagenes/residenteicon.png'),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Text(
                    'FAMILIAR',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'CenturyGothic',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.2),
                  ),
                  SizedBox(height: 15.0),
                ],
              ))
        ],
      );
    }
  }

  Widget getRol(rol) {
    return Column(
      children: <Widget>[
        FlatButton(
            color: rol['color'],
            onPressed: () {
              appData.datosAppSelecionado.permiso = rol['permission'];
              appData.rol = rol['name'].toUpperCase();
              GenericAlert(
                  context,
                  "¿Está seguro de que desea entrar con el rol de " +
                      appData.rol.toLowerCase() +
                      "?",
                  rol['name']);
            },
            child: Column(
              children: <Widget>[
                Container(
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 41.0,
                    child: Image.asset(rol['url']),
                  ),
                ),
                SizedBox(height: 5.0),
                Text(
                  rol['name'].toUpperCase(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'CenturyGothic',
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.2),
                ),
              ],
            ))
      ],
    );
  }
}
