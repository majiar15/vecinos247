import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vecinos247/src/providers/push_notification_provider.dart';

class AdminChatAdministradorPages extends StatefulWidget {
  final String cedulachatresidente;
  AdminChatAdministradorPages({this.cedulachatresidente, Key key})
      : super(key: key);

  @override
  _AdminChatAdministradorPagesState createState() =>
      _AdminChatAdministradorPagesState();
}

class _AdminChatAdministradorPagesState
    extends State<AdminChatAdministradorPages> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final messageController = TextEditingController();
  ScrollController scrollController = ScrollController();

  Future<void> callback() async {
    var documentReference = FirebaseFirestore.instance
        .collection('mensagesEdificio' +
            appData.datosAppSelecionado.id_unidad.toString())
        .doc(widget.cedulachatresidente)
        .collection(widget.cedulachatresidente)
        .doc(DateTime.now().millisecondsSinceEpoch.toString());

    String menssage = messageController.text;
    if (messageController.text.length > 0) {
      FirebaseFirestore.instance.runTransaction((transaction) async {
        await transaction.set(
          documentReference,
          {
            "de": "admin",
            "texto": menssage,
            "nombre": appData.datosAppSelecionado.nombre_residente,
            "para": "re" + widget.cedulachatresidente,
            'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
            "uid_admin": appData.datosAppSelecionado.cedula.toString()
          },
        );
      });
      PushNotificationProvider().enviarPushChat("nuevo mensaje admin", menssage,
          int.tryParse(widget.cedulachatresidente), 300);
      messageController.clear();
      scrollController.animateTo(scrollController.position.minScrollExtent,
          curve: Curves.easeOut, duration: Duration(milliseconds: 300));
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: _firestore
                  .collection('mensagesEdificio' +
                      appData.datosAppSelecionado.id_unidad.toString())
                  .doc(widget.cedulachatresidente)
                  .collection(widget.cedulachatresidente)
                  .orderBy('timestamp', descending: true)
                  .limit(20)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  List<DocumentSnapshot> docs = snapshot.data.docs;
                  List<Widget> mensajes = docs
                      .map((doc) => Mensaje(
                            de: doc.data()['de'],
                            text: doc.data()['texto'],
                            nombre: doc.data()['nombre'],
                            timestamp: doc.data()['timestamp'],
                            me: "admin" == doc.data()['de'],
                          ))
                      .toList();
                  return ListView(
                    controller: scrollController,
                    reverse: true,
                    children: <Widget>[
                      ...mensajes,
                    ],
                  );
                }
              },
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: "Ingresa nuevo mensaje",
                          border: const OutlineInputBorder(),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 20.0, 5.0, 10.0)),
                      controller: messageController,
                    ),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10.0, 0, 8.0, 5.0),
                  width: 56.0,
                  child: FloatingActionButton(
                    elevation: 2.0,
                    backgroundColor: Colors.orange,
                    child: Icon(Icons.send, size: 26.0, color: Colors.white),
                    onPressed: callback,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class Mensaje extends StatelessWidget {
  final String de;
  final String text;
  final String nombre;
  final bool me;
  final int posicion;
  final String timestamp;
  const Mensaje(
      {Key key,
      this.de,
      this.text,
      this.me,
      this.nombre,
      this.posicion,
      this.timestamp})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(10.0, 5, 10.0, 5),
        child: Column(
          crossAxisAlignment:
              me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: <Widget>[
            Material(
                color: me ? Colors.orange[400] : Colors.grey[400],
                borderRadius: BorderRadius.circular(8.0),
                elevation: 1.0,
                child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Text(text))),
            Container(
              child: Text(
                DateFormat('dd MMM kk:mm').format(
                    DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp))),
                style: TextStyle(
                    color: Colors.orange[700],
                    fontSize: 12.2,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w400),
              ),
              margin: EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
            ),
          ],
        ));
  }
}
