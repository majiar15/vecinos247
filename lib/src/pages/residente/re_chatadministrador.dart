import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/providers/push_notification_provider.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:vecinos247/src/helpers/message.dart';

class ChatAdministradorPages extends StatefulWidget {
  @override
  _ChatAdministradorPagesState createState() => _ChatAdministradorPagesState();
}

class _ChatAdministradorPagesState extends State<ChatAdministradorPages> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();
  final PushNotificationProvider pushNotificacion = PushNotificationProvider();

  Future<void> callback() async {
    var documentReference = FirebaseFirestore.instance
        .collection('mensagesEdificio' +
            appData.datosAppSelecionado.id_unidad.toString())
        .doc(appData.datosAppSelecionado.cedula.toString())
        .collection(appData.datosAppSelecionado.cedula.toString())
        .doc(DateTime.now().millisecondsSinceEpoch.toString());
    var documentReference2 = FirebaseFirestore.instance
        .collection(
            'useredificio' + appData.datosAppSelecionado.id_unidad.toString())
        .doc(appData.datosAppSelecionado.cedula.toString());

    String menssage = messageController.text;
    if (messageController.text.length > 0) {
      FirebaseFirestore.instance.runTransaction((transaction) async {
        await transaction.set(
          documentReference2,
          {
            "cedula": appData.datosAppSelecionado.cedula.toString(),
            "texto": menssage,
            "nombre": appData.datosAppSelecionado.nombre_residente,
            "subunidad": appData.datosAppSelecionado.id_subunidad,
            "nombre_subunidad": appData.datosAppSelecionado.nombre_subunidad,
            'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
          },
        );
      });
      FirebaseFirestore.instance.runTransaction((transaction) async {
        await transaction.set(
          documentReference,
          {
            "de": "re" + appData.datosAppSelecionado.cedula.toString(),
            "texto": menssage,
            "nombre": appData.datosAppSelecionado.nombre_residente,
            "subunidad": appData.datosAppSelecionado.id_subunidad,
            "nombre_subunidad": appData.datosAppSelecionado.nombre_subunidad,
            "para": "admin",
            'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
          },
        );
      });
      pushNotificacion.enviarPushChat("nuevo mensaje admin", menssage, 0, 100);
      messageController.clear();
      scrollController.animateTo(scrollController.position.minScrollExtent,
          curve: Curves.easeOut, duration: Duration(milliseconds: 300));
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: _firestore
                  .collection('mensagesEdificio' +
                      appData.datosAppSelecionado.id_unidad.toString())
                  .doc(appData.datosAppSelecionado.cedula.toString())
                  .collection(appData.datosAppSelecionado.cedula.toString())
                  .orderBy('timestamp', descending: true)
                  .limit(20)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  List<DocumentSnapshot> docs = snapshot.data.docs;
                  List<Widget> mensajes = docs.map((doc) {
                    final mensaje = Mensaje(
                      de: doc.data()['de'],
                      text: doc.data()['texto'],
                      nombre: doc.data()['nombre'],
                      timestamp: doc.data()['timestamp'],
                      me: "re" + appData.datosApp.cedula.toString() ==
                          doc.data()['de'],
                    );
                    return mensaje;
                  }).toList();
                  return ListView(
                    reverse: true,
                    controller: scrollController,
                    children: <Widget>[
                      ...mensajes,
                    ],
                  );
                }
              },
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: "Ingresa nuevo mensaje",
                          border: const OutlineInputBorder(),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 20.0, 5.0, 10.0)),
                      controller: messageController,
                    ),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10.0, 0, 8.0, 5.0),
                  width: 56.0,
                  child: FloatingActionButton(
                    elevation: 2.0,
                    backgroundColor: Colors.orange,
                    child: Icon(Icons.send, size: 32.0, color: Colors.white),
                    onPressed: callback,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class Mensaje extends StatelessWidget {
  final String de;
  final String text;
  final String nombre;
  final bool me;
  final int posicion;
  final String timestamp;
  const Mensaje(
      {Key key,
      this.de,
      this.text,
      this.me,
      this.nombre,
      this.posicion,
      this.timestamp})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(10.0, 5, 10.0, 5),
        child: Column(
          crossAxisAlignment:
              me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: <Widget>[
            Material(
                color: me ? Colors.orange[400] : Colors.grey[400],
                borderRadius: BorderRadius.circular(8.0),
                elevation: 1.0,
                child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Text(text))),
            Container(
              child: Text(
                  DateFormat('dd MMM kk:mm').format(
                      DateTime.fromMillisecondsSinceEpoch(
                          int.parse(timestamp))),
                  style: TextStyle(
                      color: Colors.orange[700],
                      fontSize: 12.2,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400)),
              margin: EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
            ),
          ],
        ));
  }
}
