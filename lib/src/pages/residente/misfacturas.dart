import 'dart:io';
// import 'package:file_picker/file_picker.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/models/facturas.dart';
import 'package:vecinos247/src/providers/facturasProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vecinos247/src/widgets/alerts.dart';

class MisFacturas extends StatefulWidget {
  @override
  _MisFacturasState createState() => _MisFacturasState();
}

class _MisFacturasState extends State<MisFacturas> {
  @override
  final facturaprovider = FacturaProvider();
  File _image;
  final formatNumber = NumberFormat("#,##0", "en_US");
  int id_factura;
  String respEnviarFactura;
  Widget build(BuildContext context) {
    return _listaFacturas(context);
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _listaFacturas(context) {
    return FutureBuilder(
        future: facturaprovider.getlistafactura(context),
        builder: (BuildContext context, AsyncSnapshot<DatosFacturas> snapshot) {
          print("snapshot");
          print(snapshot);
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data.id_factura != null) {
              return _cardMensajesFacturas(snapshot.data, context);
            } else {
              return Center(
                child: Text(
                  'En estos momentos no tiene facturas disponibles',
                  style: TextStyle(fontSize: 20.0),
                  textAlign: TextAlign.center,
                ),
              );
            }
          } else {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
              )),
            );
          }
        });
  }

  Widget _cardMensajesFacturas(DatosFacturas datos, context) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 2.0),
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 15.0,
            ),
            Center(
              child: Text(
                "INFORMACION FACTURA",
                style: TextStyle(
                    color: Color.fromRGBO(255, 153, 29, 1.0),
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            cardFactura(context, datos.conceptos, datos.total),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "Fecha",
                  style: TextStyle(fontSize: 15),
                ),
                Text(
                  "Valor",
                  style: TextStyle(fontSize: 15),
                ),
                Text(
                  "Estado",
                  style: TextStyle(fontSize: 15),
                ),
              ],
            ),
            FutureBuilder(
              future: facturaprovider.getHistory(context),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                print("====================================");
                print(snapshot.data);
                print("====================================");
                if (snapshot.hasData) {
                  return Container(
                    height: 300.0,
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int i) {
                        print(snapshot.data[i]);
                        if (snapshot.data[i]['estado'] == "PENDIENTE" ||
                            snapshot.data[i]['estado'] == "MORA") {
                          id_factura = snapshot.data[i]['id_factura'];
                        }
                        return i % 2 == 0
                            ? cardMensajes(
                                snapshot.data[i]['total'],
                                snapshot.data[i]['fecha_factura'],
                                Colors.red.shade100,
                                snapshot.data[i]['estado'].toString(),
                                snapshot.data[i]['id_factura'],
                                snapshot.data[i]['conceptos'],
                                context)
                            : cardMensajes(
                                snapshot.data[i]['total'],
                                snapshot.data[i]['fecha_factura'],
                                Colors.grey.shade300,
                                snapshot.data[i]['estado'].toString(),
                                snapshot.data[i]['id_factura'],
                                snapshot.data[i]['conceptos'],
                                context);
                      },
                    ),
                  );
                } else {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                        child: CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.orange),
                    )),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 720.0);
    setState(() {
      _image = image;
    });
  }

  Future getImageFromCamara() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 720.0);
    setState(() {
      _image = image;
    });
  }

  Future getPDF() async {
    // FilePickerResult result = await FilePicker.platform.pickFiles(
    //   type: FileType.custom,
    //   allowedExtensions: ['pdf'],
    // );
    // var image = await ImagePicker.pickImage(
    //     source: ImageSource.camera, maxHeight: 250.0, maxWidth: 250.0);
    // setState(() {
    //   _image = image;
    // });
  }

  Future<void> _showDialogOption(BuildContext context, int idFactura) {
    respEnviarFactura = '';
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Seleccione"),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text('Galeria'),
                    onTap: () async {
                      await getImageFromGallery();
                      // mandar imagen al server
                      // await facturaprovider.enviarFactura();
                      respEnviarFactura = await facturaprovider.enviarFactura(
                          context, _image, idFactura);
                      Navigator.pop(context);
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text('Camara'),
                    onTap: () async {
                      await getImageFromCamara();
                      respEnviarFactura = await facturaprovider.enviarFactura(
                          context, _image, idFactura);

                      print(respEnviarFactura);
                      // mandar imagen al server
                      Navigator.pop(context);
                    },
                  ),
                  // GestureDetector(
                  //   child: Text('PDF'),
                  //   onTap: () async {
                  //     await getPDF();
                  //     // mandar imagen al server
                  //     // await facturaprovider.enviarFactura();

                  //     Navigator.pop(context);
                  //   },
                  // ),
                ],
              ),
            ),
          );
        });
  }

  Widget _alertFacturas(BuildContext context, conceptos) {
    final entidades = new ListaConceptos.fromJsonList(conceptos);
    entidades.items.reversed.toList();
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            content: ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 30.0),
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Scrollbar(
                  child: ListView.builder(
                      itemCount: entidades.items.length,
                      itemBuilder: (context, index) {
                        return Text(entidades.items[index].concepto);
                      }),
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Aceptar',
                  style: TextStyle(color: Color.fromRGBO(205, 105, 55, 1.0)),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          );
        });
  }

  Widget cardFactura(context, data, int total) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
      ),
      elevation: 5.0,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.topRight,
                colors: [
              //Color.fromRGBO(205, 105, 55,1.0)

              Color.fromRGBO(224, 97, 0, 1.0),
              Color.fromRGBO(255, 114, 0, 1.0),
            ])),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'SU DEUDA ACTUAL ES :',
                            style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.white,
                                fontFamily: 'CenturyGothic'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 20.0,
                          ),
                          Icon(
                            Icons.monetization_on,
                            color: Colors.white,
                            size: 55.0,
                          ),
                          // Expanded(
                          //   child: Text(formatNumber.format(total).toString(),
                          //       style: TextStyle(
                          //           color: Colors.white,
                          //           fontSize: 45.0,
                          //           fontFamily: 'CenturyGothic')),
                          // ),
                          FutureBuilder(
                            future: facturaprovider.getSaldo(),
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return CircularProgressIndicator();
                              } else {
                                return Expanded(
                                  child: Text(
                                      snapshot.data['saldo_total'] == null
                                          ? "0"
                                          : formatNumber
                                              .format(
                                                  snapshot.data['saldo_total'])
                                              .toString(),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 45.0,
                                          fontFamily: 'CenturyGothic')),
                                );
                              }
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () {
                              showSimpleCustomDialog(context, data);
                            },
                            child: Text(
                              "Conceptos",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontFamily: 'CenturyGothic'),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () {
                                  showSimpleDialogPagar(context);
                                },
                                child: Text(
                                  "Pagar",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontFamily: 'CenturyGothic'),
                                ),
                              ),
                              // InkWell(
                              //   onTap: () async {
                              //     await _showDialogOption(context);
                              //     if (respEnviarFactura == 'ok') {
                              //       AlertEnvioImagenPago(
                              //         context,
                              //         "Factura enviada correctamente",
                              //       );
                              //     } else {
                              //       AlertEnvioImagenPago(
                              //         context,
                              //         "no se pudo enviar la factura",
                              //       );
                              //     }
                              //   },
                              //   child: Text(
                              //     "Adjuntar comprobante",
                              //     style: TextStyle(
                              //         fontSize: 18,
                              //         color: Colors.white,
                              //         fontFamily: 'CenturyGothic'),
                              //   ),
                              // )
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Container(
                          width: 400.0,
                          height: 5.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Colors.white))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget cardMensajes(
    total,
    String fecha,
    Color color,
    String estado,
    int idFactura,
    var conceptos,
    BuildContext context,
  ) {
    return GestureDetector(
        onTap: () {
          showSimpleDialogPagar(context, conceptos, "selectFactura", idFactura);
        },
        child: Card(
          color: color,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  fecha,
                  maxLines: null,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.grey.shade700,
                      fontFamily: 'CenturyGothic',
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0),
                ),
                SizedBox(
                  width: 15.0,
                ),
                Text(
                  formatNumber.format(total).toString(),
                  maxLines: null,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.grey.shade700,
                      fontFamily: 'CenturyGothic',
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0),
                ),
                SizedBox(
                  width: 15.0,
                ),
                Text(
                  estado,
                  maxLines: null,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.grey.shade700,
                      fontFamily: 'CenturyGothic',
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0),
                ),
              ],
            ),
          ),
        ));
  }

  void showSimpleCustomDialog(BuildContext context, conceptos) {
    final entidades = new ListaConceptos.fromJsonList(conceptos);
    entidades.items.reversed.toList();
    Dialog simpleDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Container(
        width: 500,
        height: 400,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text("Conceptos",
                  style: TextStyle(
                    fontSize: 24.0,
                    color: Color.fromRGBO(255, 153, 29, 1.0),
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                  )),
            ),
            ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 280.0),
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Scrollbar(
                  child: ListView.builder(
                      itemCount: entidades.items.length,
                      itemBuilder: (context, index) {
                        return _containerConcepto(
                            entidades.items[index].concepto,
                            entidades.items[index].valor);
                      }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => simpleDialog);
  }

  void showSimpleDialogPagar(BuildContext context,
      [var conceptos, String tipoFactura = "normal", int idFactura]) {
    Dialog simpleDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Container(
        width: 500,
        height: 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text("¿Donde pagar?",
                  style: TextStyle(
                    fontSize: 24.0,
                    color: Color.fromRGBO(255, 153, 29, 1.0),
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                  )),
            ),
            ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 220.0),
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: FutureBuilder(
                  future: facturaprovider.getLinkPago(context),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                      return CircularProgressIndicator(
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.orange),
                      );
                    } else {
                      return SelectableText(snapshot.data,
                          style: TextStyle(
                            fontSize: 19.0,
                          ));
                    }
                  },
                ),
                // child: ListView.builder(
                //     itemCount: entidades.items.length,
                //     itemBuilder: (context, index) {
                //       return _containerConcepto(
                //           entidades.items[index].concepto,
                //           entidades.items[index].valor.toString());
                //     }),
                // child: FutureBuilder(
                //   future: facturaprovider.getLinkPago(),
                //   builder: (context, snapshot) {
                //     print("asd \n");
                //     return ListView.builder(itemBuilder: (context, index) {
                //       if (!snapshot.hasData) {
                //         return CircularProgressIndicator(
                //           valueColor:
                //               new AlwaysStoppedAnimation<Color>(Colors.orange),
                //         );
                //       } else {
                //         return Text(snapshot.data,
                //             style: TextStyle(
                //               fontSize: 15.0,
                //             ));
                //       }
                //       // Text(
                //       //   "Bancolombia",
                //       //   style: TextStyle(
                //       //     fontSize: 17.0,
                //       //     color: Color.fromRGBO(255, 153, 29, 1.0),
                //       //     fontFamily: 'CenturyGothic',
                //       //     fontWeight: FontWeight.bold,
                //       //   ),
                //       // ),

                //       // SizedBox(
                //       //   height: 15,
                //       // ),
                //       // Text(
                //       //   "nequi",
                //       //   style: TextStyle(
                //       //     fontSize: 17.0,
                //       //     color: Color.fromRGBO(255, 153, 29, 1.0),
                //       //     fontFamily: 'CenturyGothic',
                //       //     fontWeight: FontWeight.bold,
                //       //   ),
                //       // ),
                //       // Text("3154646416"),
                //       // SizedBox(
                //       //   height: 15,
                //       // ),
                //       // Text(
                //       //   "daviplata",
                //       //   style: TextStyle(
                //       //     fontSize: 17.0,
                //       //     color: Color.fromRGBO(255, 153, 29, 1.0),
                //       //     fontFamily: 'CenturyGothic',
                //       //     fontWeight: FontWeight.bold,
                //       //   ),
                //       // ),
                //       // Text("3154646416"),
                //     });
                //   },
                // ),
              ),
            ),
            tipoFactura != "normal"
                ? Column(
                    children: [
                      MaterialButton(
                        onPressed: () async {
                          await _showDialogOption(context, idFactura);

                          if (respEnviarFactura == 'ok') {
                            AlertEnvioImagenPago(
                              context,
                              "Factura enviada correctamente",
                            );
                          } else {
                            AlertEnvioImagenPago(
                              context,
                              "no se pudo enviar la factura",
                            );
                          }
                        },
                        color: Color.fromRGBO(255, 153, 29, 1.0),
                        child: Text('Adjuntar comprobante',
                            style: TextStyle(color: Colors.white)),
                      ),
                      MaterialButton(
                        onPressed: () async {
                          await showSimpleCustomDialog(context, conceptos);
                        },
                        color: Color.fromRGBO(255, 153, 29, 1.0),
                        child: Text('Conceptos',
                            style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  )
                : Container()
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => simpleDialog);
  }

  Widget _containerConcepto(concepto, valor) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border:
                Border.all(width: 2, color: Color.fromRGBO(255, 153, 29, 1.0))),
        child: Column(
          children: <Widget>[
            Text(
              concepto,
              style: TextStyle(fontSize: 17.0),
            ),
            Text(
              "VALOR: " + formatNumber.format(valor),
              style: TextStyle(fontSize: 17.0),
            )
          ],
        ));
  }

  Widget _tarjeta(BuildContext context) {
    final tarjeta = SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.message),
                    title: Text('Numero factura:'),
                    subtitle: Text('3456576'),
                  ),
                  ListTile(
                    leading: Text(''),
                    title: Text('Fecha:'),
                    subtitle: Text('12/02/2020'),
                  ),
                  ListTile(
                    leading: Text(''),
                    title: Text('Valor:'),
                    subtitle: Text('10.000'),
                  ),
                  ListTile(
                    leading: Text(''),
                    title: Text('Estado:'),
                    subtitle: Text('Cancelada'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    return GestureDetector(
      child: tarjeta,
      onTap: () {},
    );
  }
}
