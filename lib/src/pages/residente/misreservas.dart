import 'dart:convert';
import 'dart:math';

import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:vecinos247/src/http/api-service.dart';
import 'package:vecinos247/src/http/datos-reserva.dart';
import 'package:vecinos247/src/models/reserva_disponibilidad.dart';
import 'package:vecinos247/src/models/visitas_models.dart';
import 'package:vecinos247/src/pages/residente/drawer.dart';
import 'package:vecinos247/src/providers/reservas_provider.dart';
// import 'package:vecinos247/src/widgets/calendar_widget.dart';
// import 'package:vecinos247/src/widgets/dropdown_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:vecinos247/src/widgets/alerts.dart';

class MyReserva extends StatefulWidget {
  MyReserva({Key key}) : super(key: key);

  @override
  MyReservaState createState() => MyReservaState();
}

class MyReservaState extends State<MyReserva> {
  final DateTime now = DateTime.now();
  String _selectedDate = '2020-06-09';
  String _idreserva = " ";
  String _dropdownStrHoraInicio = " ";
  List opcionesInicial = [" "];
  List opcionesFinal = [' '];
  String fecha_Selecionada = '2020-06-09';
  String _dropdownStrHoraFinal = " ";
  final _controllerObservaciones = TextEditingController();
  List<dynamic> _listareserva = new List();
  List<dynamic> _listaZonas = new List();
  String _zonaSelecionadad = ' ';
  int _posicionZona = 1;
  String fechainicioactualizar;
  String fechafinalactualizar;
  final reservaProvider = new ReservasProvider();
  CalendarController _calendarController = CalendarController();
  List<dynamic> _listaocupada = [];
  Map<DateTime, List<dynamic>> _events = {};
  TextEditingController _eventsController = new TextEditingController();
  List<Reserva> todasReserva = new List();
  bool _estadotablecale = true;
  bool _estadoDisponibilidadZonas = true;
  List<Reserva> _datoslistareserva = new List();
  ApiService apiService = new ApiService();
  final providerReserva = ReservasProvider();
  bool reservadoTodo = false;
  String _horaSelecionada;
  String _horaSelecionada2;
  String _zonaSelecionada;
  int _posicionSelecionadaZona = 0;
  int _posicionSelecionadoHora;
  String _fechaSelecionada;
  int _id_zona;
  List<String> _horasDisponibles2 = new List();
  @override
  Widget build(BuildContext context) {
    _solicitarTodasReserva();

    return Scaffold(
      // floatingActionButton: Draggable(
      //     feedback: Padding(
      //       padding: EdgeInsets.only(bottom: 20.0),
      //       child: FloatingActionButton(
      //         backgroundColor: Colors.white,
      //         child: Icon(
      //           Icons.info,
      //           color: Color.fromRGBO(255, 153, 29, 1.0),
      //           size: 40.0,
      //         ),
      //         onPressed: () => {
      //           alertReservaInformacion(),
      //         },
      //       ),
      //     ),
      //     childWhenDragging: Container(),
      //     child: Padding(
      //       padding: EdgeInsets.only(bottom: 20.0),
      //       child: FloatingActionButton(
      //         backgroundColor: Colors.white,
      //         child: Icon(
      //           Icons.info,
      //           color: Color.fromRGBO(255, 153, 29, 1.0),
      //           size: 40.0,
      //         ),
      //         onPressed: () => {
      //           alertReservaInformacion(),
      //         },
      //       ),
      //     ),
      //     onDragEnd: (details) => print(details.offset)),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _calendarTable(),
            SizedBox(
              height: 15,
            ),
            InkWell(
              child: Container(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromRGBO(255, 153, 29, 1.0),
                ),
                child: Text(
                  "Horario de disponibilidad",
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
              ),
              onTap: () => {
                alertReservaInformacion(),
              },
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "TODAS MIS RESERVAS REALIZADAS",
              style: TextStyle(
                  color: Color.fromRGBO(255, 153, 29, 1.0),
                  fontFamily: 'CenturyGothic',
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0),
            ),
            _reservas(context),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Container(
                height: 4.0,
                width: 350.0,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(5.0)),
              ),
            ),
            SizedBox(height: 30.0),
            botonPrincipal(context),
            SizedBox(height: 10.0),
          ],
        ),
      ),
    );
  }

  _solicitarTodasReserva() {
    reservaProvider.getreservatodas(context).then((onValue) {
      if (onValue != null) {
        _listaocupada = new List();
        _events.clear();
        _eventsController.clear();
        for (int i = 0; i < onValue.length; i++) {
          if (_events[corregir_fecha(onValue[i].fecha_hora_inicio)] != null) {
            _events[corregir_fecha(onValue[i].fecha_hora_inicio)].add(
                onValue[i].nombre_zona +
                    " \n F. inicio  " +
                    onValue[i].fecha_hora_inicio +
                    "    F. fin  " +
                    onValue[i].fecha_hora_fin);
          } else {
            _events[corregir_fecha(onValue[i].fecha_hora_inicio)] = [
              onValue[i].nombre_zona +
                  " \n F. inicio  " +
                  onValue[i].fecha_hora_inicio +
                  "    F. fin  " +
                  onValue[i].fecha_hora_fin
            ];
          }
        }
        print(_events);
      }
    });
  }

  _botonGuardar(context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
          child: RaisedButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5),
        ),
        onPressed: () {
          if (_horaSelecionada2 == "") {
            _alertReservaMensajes(context, "Tiene que elegir una hora final");
            return;
          }

          DatosReserva datosReserva;
          if (_idreserva != " ") {
            datosReserva = DatosReserva(
                id_subunidad:
                    appData.datosAppSelecionado.id_subunidad.toString(),
                valor: "5000",
                id_perfil: appData.rol == "Residente"
                    ? 100
                    : appData.rol == "Administrador"
                        ? 300
                        : 200,
                id_residente: appData.datosAppSelecionado.id.toString(),
                id_zona_social: _id_zona.toString(),
                observaciones: _controllerObservaciones.text,
                fecha_hora_inicio: _fechaSelecionada + " " + _horaSelecionada,
                fecha_hora_fin: _fechaSelecionada + " " + _horaSelecionada2,
                username: appData.datosAppSelecionado.cedula.toString(),
                id_reserva: _idreserva.toString());
          } else {
            datosReserva = DatosReserva(
                id_subunidad:
                    appData.datosAppSelecionado.id_subunidad.toString(),
                valor: "5000",
                id_perfil: appData.rol == "Residente"
                    ? 100
                    : appData.rol == "Administrador"
                        ? 300
                        : 200,
                id_residente: appData.datosAppSelecionado.id.toString(),
                id_zona_social: _id_zona.toString(),
                observaciones: _controllerObservaciones.text,
                fecha_hora_inicio: _fechaSelecionada + " " + _horaSelecionada,
                fecha_hora_fin: _fechaSelecionada + " " + _horaSelecionada2,
                username: appData.datosAppSelecionado.cedula.toString(),
                id_reserva: " ");
          }
          ApiService apiService = ApiService();
          apiService.guardarReserva(datosReserva).then((isSuccess) {
            if (isSuccess == 'ok') {
              Navigator.pop(context);
              _alertReservaMensajes(context, "Reserva exitosa");
              _idreserva = " ";
              _controllerObservaciones.text = " ";
              _estadotablecale = true;
              setState(() {});
            } else {
              _alertReservaMensajes(context, isSuccess);
            }
          });
        },
        child: const Text('ENVIAR RESERVA',
            style: TextStyle(
                fontSize: 18.0,
                color: Color.fromRGBO(255, 153, 29, 1.0),
                fontFamily: 'CenturyGothic',
                fontWeight: FontWeight.bold)),
      )),
    );
  }

  Future<void> alertReservaInformacion() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
            child: Text(
              'Reservas',
              style: TextStyle(
                color: Color.fromRGBO(255, 153, 29, 1.0),
                fontFamily: 'OpenSans',
              ),
            ),
          ),
          content: SingleChildScrollView(
            child: FutureBuilder(
                future: apiService.InformacionZona(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) print(snapshot.error);
                  return snapshot.hasData
                      ? new getZonaSocial(list: snapshot.data)
                      : new Center(
                          child: CircularProgressIndicator(),
                        );
                }),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Aceptar',
                style: TextStyle(
                  color: Color.fromRGBO(255, 153, 100, 1.0),
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _alertReservaMensajes(BuildContext context, String mensaje) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 40.0,
                  child: Image.asset('recursos/logo/logo.png'),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Text(mensaje),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Aceptar',
                  style: TextStyle(color: Color.fromRGBO(205, 105, 55, 1.0)),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          );
        });
  }

  Widget _camposFormulario(
      String texto, TextEditingController controller, TextInputType type) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(0, 4, 10, 4),
        height: 30,
        child: TextField(
          controller: controller,
          autofocus: false,
          keyboardType: type,
          style: new TextStyle(
            fontSize: 13.0,
            color: Colors.black,
            fontFamily: 'CenturyGothic',
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.justify,
          decoration: new InputDecoration(
            filled: true,
            //hintStyle: TextStyle(color: Colors.white54, fontFamily: 'CenturyGothic'),
            fillColor: Color.fromRGBO(233, 233, 233, 1),
            hintText: texto,
            // hoverColor: Colors.black,
            // focusColor: Colors.black,
            contentPadding:
                const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
            focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius: new BorderRadius.circular(5),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius: new BorderRadius.circular(5),
            ),
          ),
        ));
  }

  Widget _camposFormulario3(
      String texto, TextEditingController controller, TextInputType type) {
    return Center(
      child: Container(
          width: double.infinity,
          child: TextField(
            controller: controller,
            autofocus: false,
            keyboardType: type,
            style: new TextStyle(
              fontSize: 13.0,
              color: Colors.black,
              fontFamily: 'CenturyGothic',
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.justify,
            decoration: new InputDecoration(
              filled: true,
              fillColor: Color.fromRGBO(233, 233, 233, 1),
              hintText: texto,
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.white),
                borderRadius: new BorderRadius.circular(5),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: new BorderSide(color: Colors.white),
                borderRadius: new BorderRadius.circular(5),
              ),
            ),
          )),
    );
  }

  _colocarHoraDisponibles(setState) {
    var fecha = _dropdownStrHoraInicio;
    var primeroSegundoCaracter = fecha.substring(0, 2);
    List datos = List<String>();

    for (int i = 2; i < opcionesInicial.length; i++) {
      datos.add(opcionesInicial[i]);
    }
    datos.add(" ");
    setState(() {
      opcionesFinal = datos;
    });
  }

  _reservas(BuildContext context) {
    if (_estadotablecale == true) {
      return FutureBuilder(
          future: reservaProvider.getAllReservas(context),
          builder:
              (BuildContext context, AsyncSnapshot<List<Reserva>> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              _datoslistareserva = snapshot.data;
              List<Reserva> newSnapshot = snapshot.data.reversed.toList();
              return ConstrainedBox(
                constraints: new BoxConstraints(
                  maxHeight: 160.0,
                ),
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  child: Scrollbar(
                    child: new ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        Color _colorcelda;
                        if (newSnapshot[index].estado == "En espera") {
                          _colorcelda = Color.fromRGBO(244, 208, 63, 1);
                        } else if (newSnapshot[index].estado == "NO aprobado" ||
                            newSnapshot[index].estado ==
                                "NO Aprobado por Admin") {
                          _colorcelda = Color.fromRGBO(231, 76, 60, 1);
                        } else {
                          _colorcelda = Color.fromRGBO(34, 153, 84, 1);
                        }
                        return _cardMensajes(
                            newSnapshot[index].nombre_zona.toString(),
                            newSnapshot[index].fecha_hora_inicio,
                            newSnapshot[index].fecha_hora_fin,
                            newSnapshot[index].fecha_hora_fin,
                            newSnapshot[index].estado,
                            newSnapshot[index].id_reserva,
                            _colorcelda,
                            context,
                            newSnapshot[index].observaciones.toString(),
                            newSnapshot[index].id_zona_social);
                      },
                    ),
                  ),
                ),
              );
            } else {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
                )),
              );
            }
          });
    } else {
      return ConstrainedBox(
        constraints: new BoxConstraints(
          maxHeight: 180.0,
        ),
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Scrollbar(
            child: new ListView.builder(
              itemCount: _datoslistareserva.length,
              itemBuilder: (context, index) {
                Color _colorcelda;
                if (_datoslistareserva[index].estado == "En espera") {
                  _colorcelda = Colors.yellow;
                } else if (_datoslistareserva[index].estado == "NO aprobado") {
                  _colorcelda = Colors.red;
                } else {
                  _colorcelda = Colors.green;
                }
                print(_datoslistareserva[index]);
                print(_datoslistareserva[index].fecha_hora_fin);
                return _cardMensajes(
                    _datoslistareserva[index].nombre_zona.toString(),
                    _datoslistareserva[index].fecha_hora_inicio,
                    _datoslistareserva[index].fecha_hora_fin,
                    _datoslistareserva[index].fecha_hora_fin,
                    _datoslistareserva[index].estado,
                    _datoslistareserva[index].id_reserva,
                    _colorcelda,
                    context,
                    _datoslistareserva[index].observaciones.toString(),
                    _datoslistareserva[index].id_zona_social);
              },
            ),
          ),
        ),
      );
    }
  }

  Widget _cardMensajestodareserva(texto, color) {
    return GestureDetector(
        child: Card(
      color: color,
      child: Container(
        height: 30.0,
        padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 2.0),
        child: Row(
          children: <Widget>[
            Text(
              texto,
              style: TextStyle(
                  color: Colors.grey.shade700,
                  fontFamily: 'CenturyGothic',
                  fontWeight: FontWeight.bold,
                  fontSize: 10.0),
            ),
          ],
        ),
      ),
    ));
  }

  Widget _cardMensajes(texto, fechainicio, fechafin, hora, estado, id_reserva,
      color, BuildContext context, String observacion, int id_zona_social) {
    return GestureDetector(
      child: Card(
        color: color,
        child: Container(
          height: 50,
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 2.0),
          child: Row(
            children: <Widget>[
              Text(
                texto,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                    fontSize: 10.0),
              ),
              SizedBox(
                width: 15.0,
              ),
              Text(
                fechainicio,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                    fontSize: 10.0),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      "Fecha de fin: " + fechafin,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'CenturyGothic',
                          fontWeight: FontWeight.bold,
                          fontSize: 10.0),
                    ),
                    Text(
                      estado,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'CenturyGothic',
                          fontWeight: FontWeight.bold,
                          fontSize: 10.0),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      onTap: () {
        _alertLista(texto, fechainicio, fechafin, observacion, estado,
            id_reserva, id_zona_social);
      },
    );
  }

  Widget _alertLista(String tipo, String fechainicio, String fechafin,
      String observacion, String hora, id_reserva, int id_zona_social) {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
            child: Text('RESERVA'),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                _campoAlert(tipo),
                _campoAlert("INICIO: " + fechainicio),
                _campoAlert("FIN: " + fechafin),
                _campoAlert(hora),
                _campoAlert(observacion.toString() != "null"
                    ? observacion
                    : "observacion"),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Eliminar'),
              onPressed: () {
                _idreserva = " ";
                ApiService apiService = new ApiService();
                apiService
                    .borrarReserva(id_reserva, context)
                    .then((onValue) async {
                  print("==============================");
                  print(onValue);
                  Navigator.of(context).pop();
                  await _genericAlert(context, onValue['msj'], null);
                  setState(() {});
                });
              },
            ),
            FlatButton(
              child: Text('Editar'),
              onPressed: () {
                Navigator.of(context).pop();
                String _year = fechainicio.substring(6, 10);
                String _month = fechainicio.substring(3, 5);
                String _day = fechainicio.substring(0, 2);
                _selectedDate = _day + "/" + _month + "/" + _year;
                fechafinalactualizar = fechafin;
                fechainicioactualizar = fechainicio;
                _idreserva = id_reserva.toString();
                _selectedDate = " ";
                _posicionZona = id_zona_social;
                _controllerObservaciones.text = observacion;
              },
            ),
            FlatButton(
              child: Text('Cerrar'),
              onPressed: () {
                _idreserva = " ";
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _campoAlert(String texto) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(6, 4, 10, 4),
      padding: EdgeInsets.only(left: 7),
      height: 25,
      decoration: BoxDecoration(
        color: Color.fromRGBO(233, 233, 233, 1),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Text(
              texto,
              style: TextStyle(
                fontSize: 13.0,
                color: Colors.grey,
                fontFamily: 'CenturyGothic',
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
    // return Container(
    //     width: double.infinity,
    //     margin: EdgeInsets.fromLTRB(6, 4, 10, 4),
    //     height: 25,
    //     child: TextField(
    //       autofocus: false,
    //       keyboardType: TextInputType.text,
    //       style: new TextStyle(
    //         fontSize: 13.0,
    //         color: Colors.black,
    //         fontFamily: 'CenturyGothic',
    //         fontWeight: FontWeight.bold,
    //       ),
    //       textAlign: TextAlign.justify,
    //       decoration: new InputDecoration(
    //         filled: true,
    //         enabled: false,
    //         //hintStyle: TextStyle(color: Colors.white54, fontFamily: 'CenturyGothic'),
    //         fillColor: Color.fromRGBO(233, 233, 233, 1),
    //         hintText: texto,
    //         // hoverColor: Colors.black,
    //         // focusColor: Colors.black,
    //         contentPadding:
    //             const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
    //         focusedBorder: OutlineInputBorder(
    //           borderSide: new BorderSide(color: Colors.white),
    //           borderRadius: new BorderRadius.circular(5),
    //         ),
    //         enabledBorder: UnderlineInputBorder(
    //           borderSide: new BorderSide(color: Colors.white),
    //           borderRadius: new BorderRadius.circular(5),
    //         ),
    //       ),
    //     ));
  }

  botonPrincipal(context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => DrawerItem()));
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 80.0),
        padding: EdgeInsets.symmetric(vertical: 7.0),
        child: Center(
          child: Text(
            'PRINCIPAL',
            style: TextStyle(
                color: Color.fromRGBO(255, 153, 29, 1.0),
                fontFamily: 'CenturyGothic',
                fontWeight: FontWeight.bold,
                fontSize: 20.0),
          ),
        ),
        decoration: BoxDecoration(
            color: Colors.grey.shade300,
            borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }

  _calendarTable() {
    double _medidaslist = 100.0;
    return Column(
      children: <Widget>[
        TableCalendar(
          events: _events,
          locale: 'es_ES',
          initialCalendarFormat: CalendarFormat.month,
          availableCalendarFormats: {
            CalendarFormat.month: 'Mes',
            CalendarFormat.twoWeeks: '2 Semanas',
            CalendarFormat.week: 'Semana'
          },
          calendarStyle: CalendarStyle(
              canEventMarkersOverflow: true,
              todayColor: Colors.orange,
              selectedColor: Theme.of(context).primaryColor,
              todayStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0,
                  color: Colors.white)),

          headerStyle: HeaderStyle(
            centerHeaderTitle: true,
            formatButtonDecoration: BoxDecoration(
              color: Colors.orange,
              borderRadius: BorderRadius.circular(20.0),
            ),
            formatButtonTextStyle: TextStyle(color: Colors.white),
            formatButtonShowsNext: false,
          ),
          startingDayOfWeek: StartingDayOfWeek.monday,
          // onDaySelected: (date, events) {
          //   _estadotablecale = false;
          //   setState(() {
          //     _listaocupada = events;
          //   });
          // },

          onDaySelected: (day, events, holidays) {
            _estadotablecale = false;
            /* setState(() {
              _listaocupada = events;
            });*/
            _horaSelecionada = null;
            _zonaSelecionada = null;
            _fechaSelecionada = "${day.year}-${day.month}-${day.day}";
            _controllerObservaciones.text = "";
            _showDialogoReserva(_fechaSelecionada);
          },
          builders: CalendarBuilders(
            selectedDayBuilder: (context, date, events) => Container(
                margin: const EdgeInsets.all(4.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(10.0)),
                child: Text(
                  date.day.toString(),
                  style: TextStyle(color: Colors.white),
                )),
            todayDayBuilder: (context, date, events) => Container(
                margin: const EdgeInsets.all(4.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(10.0)),
                child: Text(
                  date.day.toString(),
                  style: TextStyle(color: Colors.white),
                )),
          ),
          calendarController: _calendarController,
        ),
        SizedBox(
          height: 15,
        ),
        /* Text(
          "TODAS LA RESERVAS REALIZADAS",
          style: TextStyle(
              color: Color.fromRGBO(255, 153, 29, 1.0),
              fontFamily: 'CenturyGothic',
              fontWeight: FontWeight.bold,
              fontSize: 20.0),
        ),
        ConstrainedBox(
          constraints: new BoxConstraints(
            maxHeight: _medidaslist,
          ),
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: Scrollbar(
              child: _listaocupada.length != 0
                  ? ListView.builder(
                      itemCount: _listaocupada.length,
                      itemBuilder: (context, index) {
                        return _cardMensajestodareserva(
                            _listaocupada[index], Colors.red.shade100);
                      })
                  : Text('Esta fecha no se ha hecho reserva'),
            ),
          ),
        ),*/
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25.0),
          child: Container(
            height: 4.0,
            width: 350.0,
            decoration: BoxDecoration(
                color: Colors.grey, borderRadius: BorderRadius.circular(5.0)),
          ),
        ),
      ],
    );
  }

  _showDialogoReserva(String fecha) {
    print("before ajax");
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (acontext, setState) {
            return AlertDialog(
              title: Center(
                child: FutureBuilder(
                  future:
                      providerReserva.getDisponibilidadReserva(context, fecha),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<DisponibilidadReserva>> snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      print("============================= turbo");
                      if (snapshot.data != null) {
                        if (snapshot.data.length > 0) {
                          try {
                            if (snapshot.data[_posicionSelecionadaZona]
                                    .disponibilidades ==
                                null) {
                              reservadoTodo = true;

                              print("true in try catch");
                            } else {
                              print("false else  try catch");
                              reservadoTodo = false;
                            }
                          } catch (e) {
                            print("false in  catch");
                            reservadoTodo = false;
                          }
                          if (reservadoTodo) {
                            return Center(
                              child: Text(
                                'Esta fecha no tiene disponibilidad para reserva',
                                textAlign: TextAlign.center,
                              ),
                            );
                          }
                          if (_zonaSelecionada == null) {
                            _horaSelecionada = snapshot.data[0]
                                .disponibilidades[0]['horas_disponibles'];
                            _zonaSelecionada = snapshot.data[0].nombre_zona;
                            _id_zona = snapshot.data[0].id_zona;
                            _posicionSelecionadaZona = 0;
                            _posicionSelecionadoHora = 0;
                            _horaSelecionada2 = snapshot.data[0]
                                .disponibilidades[0 + 1]['horas_disponibles'];
                            _horasDisponibles2.clear();
                            for (int i = 1;
                                i < snapshot.data[0].disponibilidades.length;
                                i++) {
                              String a = snapshot.data[0].disponibilidades[i]
                                  ['horas_disponibles'];
                              _horasDisponibles2.add(a);
                            }
                            print(snapshot
                                .data[_posicionSelecionadaZona].nombre_zona);

                            return Column(
                              children: [
                                DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                  value: _zonaSelecionada,
                                  onChanged: (String value) {
                                    _horaSelecionada = null;
                                    _zonaSelecionada = value;
                                    int a = 0;
                                    for (var item in snapshot.data) {
                                      if (item.nombre_zona == value) {
                                        _posicionSelecionadaZona = a;
                                      }
                                      a++;
                                    }
                                    _id_zona = snapshot
                                        .data[_posicionSelecionadaZona].id_zona;
                                    _posicionSelecionadoHora = 0;
                                    _horasDisponibles2.clear();
                                    setState(() {});
                                  },
                                  items: snapshot.data.map((e) {
                                    return DropdownMenuItem<String>(
                                      value: e.nombre_zona,
                                      child: Text(e.nombre_zona),
                                    );
                                  }).toList(),
                                )),
                                Column(
                                  children: [
                                    Text('Hora Inicio: ',
                                        style: TextStyle(
                                            fontFamily: 'CenturyGothic',
                                            color: Color.fromRGBO(
                                                255, 153, 29, 1.0),
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold)),
                                    DropdownButton<String>(
                                      icon: Icon(Icons.arrow_drop_down,
                                          color: Colors.orange),
                                      value: _horaSelecionada,
                                      onChanged: (String newValue) {
                                        _horaSelecionada = newValue;
                                        int a = 0;
                                        for (var item in snapshot
                                            .data[_posicionSelecionadaZona]
                                            .disponibilidades) {
                                          if (item['horas_disponibles'] ==
                                              newValue) {
                                            _posicionSelecionadoHora = a;
                                          }
                                          a++;
                                        }
                                        _horaSelecionada2 = snapshot
                                                .data[_posicionSelecionadaZona]
                                                .disponibilidades[
                                            _posicionSelecionadoHora +
                                                1]['horas_disponibles'];
                                        _horasDisponibles2.clear();
                                        for (int i =
                                                _posicionSelecionadoHora + 1;
                                            i <
                                                snapshot
                                                    .data[
                                                        _posicionSelecionadaZona]
                                                    .disponibilidades
                                                    .length;
                                            i++) {
                                          String a = snapshot
                                                  .data[_posicionSelecionadaZona]
                                                  .disponibilidades[i]
                                              ['horas_disponibles'];
                                          _horasDisponibles2.add(a);
                                        }
                                        setState(() {});
                                      },
                                      items: snapshot
                                          .data[_posicionSelecionadaZona]
                                          .disponibilidades
                                          .map((value) {
                                        return new DropdownMenuItem<String>(
                                          value: value['horas_disponibles'],
                                          child: new Text(
                                              value['horas_disponibles']),
                                        );
                                      }).toList(),
                                    ),
                                    Text('Hora Final: ',
                                        style: TextStyle(
                                            fontFamily: 'CenturyGothic',
                                            color: Color.fromRGBO(
                                                255, 153, 29, 1.0),
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold)),
                                    DropdownButton<String>(
                                      icon: Icon(Icons.arrow_drop_down,
                                          color: Colors.orange),
                                      value: _horaSelecionada2,
                                      onChanged: (String newValue) {
                                        _horaSelecionada2 = newValue;
                                        setState(() {});
                                      },
                                      items: _horasDisponibles2.map((value) {
                                        return new DropdownMenuItem<String>(
                                          value: value,
                                          child: new Text(value),
                                        );
                                      }).toList(),
                                    ),
                                    Text('OBSERVACIONES',
                                        style: TextStyle(
                                            fontFamily: 'CenturyGothic',
                                            color: Color.fromRGBO(
                                                255, 153, 29, 1.0),
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold)),
                                    _camposFormulario3(
                                        "",
                                        _controllerObservaciones,
                                        TextInputType.text),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    _botonGuardar(context)
                                  ],
                                )
                              ],
                            );
                          } else {
                            print(snapshot
                                .data[_posicionSelecionadaZona].nombre_zona);
                            return Column(
                              children: [
                                DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                  value: _zonaSelecionada,
                                  onChanged: (String value) {
                                    _horaSelecionada = null;
                                    _zonaSelecionada = value;
                                    int a = 0;
                                    for (var item in snapshot.data) {
                                      if (item.nombre_zona == value) {
                                        _posicionSelecionadaZona = a;
                                      }
                                      a++;
                                    }
                                    _id_zona = snapshot
                                        .data[_posicionSelecionadaZona].id_zona;
                                    _posicionSelecionadoHora = 0;
                                    _horasDisponibles2.clear();
                                    setState(() {});
                                  },
                                  items: snapshot.data.map((e) {
                                    return DropdownMenuItem<String>(
                                      value: e.nombre_zona,
                                      child: Text(e.nombre_zona),
                                    );
                                  }).toList(),
                                )),
                                Column(
                                  children: [
                                    Text('Hora Inicio: ',
                                        style: TextStyle(
                                            fontFamily: 'CenturyGothic',
                                            color: Color.fromRGBO(
                                                255, 153, 29, 1.0),
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold)),
                                    DropdownButton<String>(
                                      icon: Icon(Icons.arrow_drop_down,
                                          color: Colors.orange),
                                      value: _horaSelecionada,
                                      onChanged: (String newValue) {
                                        //_horasIniciales
                                        int a = 0;
                                        for (var item in snapshot
                                            .data[_posicionSelecionadaZona]
                                            .disponibilidades) {
                                          if (item['horas_disponibles'] ==
                                              newValue) {
                                            _posicionSelecionadoHora = a;
                                          }
                                          a++;
                                        }
                                        _horaSelecionada = newValue;
                                        _horasDisponibles2.clear();
                                        for (int i =
                                                _posicionSelecionadoHora + 1;
                                            i <
                                                snapshot
                                                    .data[
                                                        _posicionSelecionadaZona]
                                                    .disponibilidades
                                                    .length;
                                            i++) {
                                          String a = snapshot
                                                  .data[_posicionSelecionadaZona]
                                                  .disponibilidades[i]
                                              ['horas_disponibles'];
                                          _horasDisponibles2.add(a);
                                        }
                                        _horaSelecionada2 = snapshot
                                                .data[_posicionSelecionadaZona]
                                                .disponibilidades[
                                            _posicionSelecionadoHora +
                                                1]['horas_disponibles'];
                                        setState(() {});
                                      },
                                      items: snapshot
                                          .data[_posicionSelecionadaZona]
                                          .disponibilidades
                                          .map((value) {
                                        return new DropdownMenuItem<String>(
                                          value: value['horas_disponibles'],
                                          child: new Text(
                                              value['horas_disponibles']),
                                        );
                                      }).toList(),
                                    ),
                                    Text('Hora Final: ',
                                        style: TextStyle(
                                            fontFamily: 'CenturyGothic',
                                            color: Color.fromRGBO(
                                                255, 153, 29, 1.0),
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold)),
                                    DropdownButton<String>(
                                      icon: Icon(Icons.arrow_drop_down,
                                          color: Colors.orange),
                                      value: _horaSelecionada2,
                                      onChanged: (String newValue) {
                                        _horaSelecionada2 = newValue;
                                        setState(() {});
                                      },
                                      items: _horasDisponibles2.map((value) {
                                        return new DropdownMenuItem<String>(
                                          value: value,
                                          child: new Text(value),
                                        );
                                      }).toList(),
                                    ),
                                    Text('OBSERVACIONES',
                                        style: TextStyle(
                                            fontFamily: 'CenturyGothic',
                                            color: Color.fromRGBO(
                                                255, 153, 29, 1.0),
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold)),
                                    _camposFormulario3(
                                        "",
                                        _controllerObservaciones,
                                        TextInputType.text),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    _botonGuardar(context)
                                  ],
                                )
                              ],
                            );
                          }
                        } else {
                          return Center(
                            child: Text(
                              'Esta fecha no tiene disponibilidad para reserva',
                              textAlign: TextAlign.center,
                            ),
                          );
                        }
                      } else {
                        return Center(
                          child: Text(
                            'Esta fecha no tiene disponibilidad para reserva',
                            textAlign: TextAlign.center,
                          ),
                        );
                      }
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                            child: CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.orange),
                        )),
                      );
                    }
                  },
                ),
              ),
            );
          });
        });
  }

  DateTime corregir_fecha(String fecha) {
    var dia = fecha.substring(0, 2);
    var mes = fecha.substring(3, 5);
    var anos = fecha.substring(6, 10);
    var fechar_corregida = "${anos}-${mes}-${dia}";
    return DateTime.parse(fechar_corregida);
  }

  Future _genericAlert(BuildContext context, String texto, rol) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 50.0,
                  child: Image(
                    image: Image.asset('recursos/logo/logo.png').image,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(texto),
              ],
            ),
            actions: <Widget>[
              rol != null
                  ? FlatButton(
                      child: Text(
                        'Cancelar',
                        style:
                            TextStyle(color: Color.fromRGBO(205, 105, 55, 1.0)),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },

                      /* onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ListaPeticionesReservaPage() ),
              )*/
                    )
                  : null,
              FlatButton(
                child: Text(
                  'Aceptar',
                  style: TextStyle(color: Color.fromRGBO(205, 105, 55, 1.0)),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}

class getZonaSocial extends StatefulWidget {
  @override
  final list;
  getZonaSocial({this.list});
  _getZonaSocialState createState() => _getZonaSocialState();
}

Widget DiasZona(String dias) {
  var diaDecode = jsonDecode(dias);
  return Container(
    child: Column(
      children: [
        for (int v = 0; v < diaDecode.length; v++) ...[
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(diaDecode[v]["dia"]),
                Text(' - '),
                Text(
                  diaDecode[v]["hora_inicio"].toString().split(':')[0] +
                      ':' +
                      diaDecode[v]["hora_inicio"].toString().split(':')[1],
                ),
                // Text(
                //   dateFormat.format(DateTime(diaDecode[v]["hora_inicio"])),
                // ),
                Text(' - '),
                Text(
                  diaDecode[v]["hora_fin"].toString().split(':')[0] +
                      ':' +
                      diaDecode[v]["hora_fin"].toString().split(':')[1],
                ),
              ],
            ),
          )
        ]
      ],
    ),
  );
}

class _getZonaSocialState extends State<getZonaSocial> {
  @override
  Widget build(BuildContext context) {
    try {
      return Column(
        children: [
          Container(
            width: 100,
            height: 100,
            child: Image.asset('recursos/logo/logo.png'),
          ),
          for (int i = 0; i < widget.list.length; i++) ...[
            Text(widget.list[i]["nom_zona"]),
            SizedBox(
              height: 7,
            ),
            DiasZona(widget.list[i]["dias"]),
            Text(" "),
          ]
        ],
      );
    } catch (e) {
      return Center(
        child: Text(
          "No hay Zonas sociales",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      );
    }
  }
}
