import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';

import 'package:vecinos247/src/constantes.dart';
import 'package:vecinos247/src/http/api-service.dart';
import 'package:vecinos247/src/http/datos-visitas.dart';
import 'package:vecinos247/src/models/visita.dart';
import 'package:vecinos247/src/providers/visitaProvider.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:vecinos247/src/helpers/appdata.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:ui' as ui;
import 'package:dio/dio.dart' as dioClient;
import 'package:vecinos247/src/widgets/estadoPqrAdmin.dart';
import 'package:vecinos247/src/widgets/heroPhotoView.dart';

class VisitaPages extends StatefulWidget {
  @override
  _VisitaPagesState createState() => _VisitaPagesState();
}

final Color colorApp2 = Color.fromRGBO(0, 25, 68, 1);
DateFormat dateFormat = new DateFormat('dd/MM/yyyy hh:mm a');
DateTime now = DateTime.now();
DateTime _date = DateTime.now();
TimeOfDay _time = TimeOfDay.now();
DateTime datetime;
TimeOfDay picked;

String formattedDate = dateFormat.format(_date);
String _selectedDate = dateFormat.format(_date);

class _VisitaPagesState extends State<VisitaPages> {
  final _nombreController = TextEditingController();
  final _identificacionController = TextEditingController();
  final _fechaController = TextEditingController();
  final _observacionvisitaController = TextEditingController();
  final horaController = TextEditingController();
  final _numeroContantoController = TextEditingController();
  final _placaController = TextEditingController();
  final _solicitarVisitas = new VisitaProvider();
  final String urlImage = constantes.urlImage;

  String id, nombre, observacion, contacto, placa;
  List<DatosVisita> _listavisita = new List();
  bool _estadolistareserva = true;
  GlobalKey _globalKey = new GlobalKey();

  bool _progresgenerarvisita = false;
  File _image;
  String estado = "ADJUNTAR FOTO";

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 720.0);
    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  child: Text("GENERAR VISITA",
                      style: TextStyle(
                          fontSize: 24.0,
                          color: Color.fromRGBO(205, 105, 55, 1.0),
                          fontFamily: 'CenturyGothic',
                          fontWeight: FontWeight.bold)),
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10)),
              _camposFormulario3('IDENTIFICACION', _identificacionController,
                  TextInputType.number),
              _camposFormulario3(
                  'NOMBRE VISITANTE', _nombreController, TextInputType.text),
              _camposFormulario3('NUMERO DE CONTACTO',
                  _numeroContantoController, TextInputType.number),
              _camposFormulario3(
                  'PLACA DE AUTO', _placaController, TextInputType.text),
              _camposFormulario3('OBSERVACIONES', _observacionvisitaController,
                  TextInputType.text),
              Container(
                  margin: EdgeInsets.fromLTRB(8, 4, 8, 4),
                  child: _calendario()),
              GestureDetector(
                onTap: () {
                  getImageFromGallery();
                },
                child: Container(
                    width: size.width * 1,
                    padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                    child: Column(
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: size.width * 0.74,
                                height: 140.0,
                                color: Colors.grey.shade200,
                                child: _image == null
                                    ? Center(
                                        child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(Icons.attach_file),
                                          Text(
                                            'ADJUNTAR FOTO',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Color.fromRGBO(
                                                    189, 186, 186, 1),
                                                fontFamily: 'CenturyGothic',
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ))
                                    : Image.file(_image,
                                        fit: BoxFit.contain,
                                        filterQuality: FilterQuality.high),
                              ),
                            ]),
                        Container(
                            width: size.width * 0.4,
                            padding:
                                EdgeInsets.fromLTRB(size.width * 0.05, 4, 5, 4),
                            child: RaisedButton(
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(5)),
                              onPressed: () async {
                                if (_nombreController.text.length > 1) {
                                  if (_identificacionController.text.length <
                                          6 ||
                                      _identificacionController.text.length >
                                          10) {
                                    _alertVisitaMensajes(context,
                                        "Ingrese un numero de cedula valido");
                                    return;
                                  }
                                  if (_validarnumeros(
                                          _identificacionController.text) ==
                                      false) {
                                    _alertVisitaMensajes(context,
                                        'solo puede ingresas numeros en identificacion');
                                    return;
                                  }
                                  if (_validarnumeros(
                                          _numeroContantoController.text) ==
                                      false) {
                                    _alertVisitaMensajes(context,
                                        'solo puede ingresas numeros en numero contacto');
                                    return;
                                  }

                                  _progresgenerarvisita = true;
                                  final fecha =
                                      new DateFormat('yyyy-MM-dd hh:mm')
                                          .format(_date);
                                  final fotoVisita = _image != null
                                      ? await dioClient.MultipartFile.fromFile(
                                          _image.path)
                                      : null;
                                  DatosVisitas datosVisitas = DatosVisitas(
                                      id_visitante: "",
                                      id_residente:
                                          '${appData.datosAppSelecionado.id}',
                                      cedula_visitante:
                                          _identificacionController.text,
                                      nombre_visita: _nombreController.text,
                                      placa_auto: _placaController.text,
                                      fecha_visita: fecha,
                                      observaciones:
                                          _observacionvisitaController.text,
                                      username:
                                          '${appData.datosAppSelecionado.cedula}',
                                      estado: '${appData.estado}',
                                      foto: _image != null
                                          ? await dioClient.MultipartFile
                                              .fromFile(_image.path)
                                          : null,
                                      subunidad: appData
                                          .datosAppSelecionado.id_subunidad
                                          .toString(),
                                      token: appData.datosAppSelecionado.token);
                                  print("datosVisitas.id_visitante");
                                  print(datosVisitas.id_visitante);
                                  print("datosVisitas.id_residente");
                                  print(datosVisitas.id_residente);
                                  print("datosVisitas.cedula_visitante");
                                  print(datosVisitas.cedula_visitante);
                                  print("datosVisitas.nombre_visita");
                                  print(datosVisitas.nombre_visita);
                                  print("datosVisitas.placa_auto");
                                  print(datosVisitas.placa_auto);
                                  print("datosVisitas.fecha_visita");
                                  print(datosVisitas.fecha_visita);
                                  print("datosVisitas.observaciones");
                                  print(datosVisitas.observaciones);
                                  print("datosVisitas.username");
                                  print(datosVisitas.username);
                                  print("datosVisitas.estado");
                                  print(datosVisitas.estado);
                                  print("datosVisitas.foto");
                                  print(datosVisitas.foto);
                                  print("datosVisitas.token");
                                  print(datosVisitas.token);
                                  ApiService _apiService = new ApiService();
                                  _apiService
                                      .createProfile(datosVisitas.toJson())
                                      .then((isSuccess) {
                                    if (isSuccess) {
                                      _estadolistareserva = true;
                                      _progresgenerarvisita = false;

                                      // showSimpleCustomDialogpqr(
                                      //     context,
                                      //     _identificacionController.text
                                      //             .toString() +
                                      //         "&" +
                                      //         fecha);
                                      showSimpleCustomDialog(
                                          context,
                                          _nombreController.text,
                                          fecha,
                                          _identificacionController.text,
                                          _image);
                                      setState(() {
                                        _nombreController.text = "";
                                        _identificacionController.text = "";
                                        _fechaController.text = "";
                                        _observacionvisitaController.text = "";
                                        _numeroContantoController.text = "";
                                        _image = null;
                                      });
                                    } else {
                                      _progresgenerarvisita = false;
                                      _alertVisitaMensajes(context,
                                          "Ha ocurrido un error intentar del nuevo ");
                                    }
                                  });
                                } else {
                                  _alertVisitaMensajes(context,
                                      "No se puede dejar campos vacíos");
                                }
                              },
                              child: FittedBox(
                                child: Text('GENERAR',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color:
                                            Color.fromRGBO(205, 105, 55, 1.0))),
                              ),
                            ))
                      ],
                    )),
              ),
              Visibility(
                visible: _progresgenerarvisita,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Center(
                      child: CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Colors.orange),
                  )),
                ),
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Column(
                    children: <Widget>[
                      Text("LISTA DE VISITA",
                          style: TextStyle(
                              fontSize: 24.0,
                              color: Color.fromRGBO(205, 105, 55, 1.0),
                              fontFamily: 'CenturyGothic',
                              fontWeight: FontWeight.bold)),
                      _tablaVisita(context),
                    ],
                  )),
            ],
          )
        ],
      ),
    );
  }

  Widget _alertVisitaMensajes(BuildContext context, String mensaje) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 40.0,
                  child: Image.asset('recursos/logo/logo.png'),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Text(mensaje),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Aceptar',
                  style: TextStyle(color: Color.fromRGBO(205, 105, 55, 1.0)),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          );
        });
  }

  Widget _tablaVisita(BuildContext context) {
    if (_estadolistareserva == true) {
      return FutureBuilder(
          future: _solicitarVisitas.getlistavisitas(context),
          builder: (BuildContext context,
              AsyncSnapshot<List<DatosVisita>> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data.length != 0) {
                _listavisita = snapshot.data;
                _estadolistareserva = false;
                return ConstrainedBox(
                  constraints: new BoxConstraints(
                    maxHeight: 180.0,
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: Scrollbar(
                      child: new ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return _cardMensajes(
                              snapshot.data[index].nombre_visitante.toString(),
                              'ACTIVA',
                              snapshot.data[index].fecha,
                              snapshot.data[index].identificacion.toString(),
                              Colors.red.shade50,
                              context,
                              snapshot.data[index].foto);
                        },
                      ),
                    ),
                  ),
                );
              } else {
                return Text("No tiene visitas");
              }
            } else {
              return Column(
                children: [
                  Text("Se están cargando todas sus visitas"),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                        child: CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.orange),
                    )),
                  )
                ],
              );
            }
          });
    } else if (_listavisita.length != 0) {
      return ConstrainedBox(
        constraints: new BoxConstraints(
          maxHeight: 180.0,
        ),
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Scrollbar(
            child: new ListView.builder(
              itemCount: _listavisita.length,
              itemBuilder: (context, index) {
                return _cardMensajes(
                    _listavisita[index].nombre_visitante.toString(),
                    'ACTIVA',
                    _listavisita[index].fecha,
                    _listavisita[index].identificacion.toString(),
                    Colors.red.shade100,
                    context,
                    _listavisita[index].foto);
              },
            ),
          ),
        ),
      );
    } else {
      _estadolistareserva = false;
      return Text("No tiene visitas");
    }
  }

  Widget _cardMensajes(
      texto, estado, fecha, id, color, BuildContext context, imagen) {
    return GestureDetector(
      child: Card(
        color: color,
        elevation: 0.4,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 14.0, vertical: 10.0),
          child: Row(
            children: <Widget>[
              Text(
                texto,
                style: TextStyle(
                    color: Colors.grey.shade700,
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0),
              ),
              SizedBox(width: 15.0),
              Text(
                '(LEER)',
                style: TextStyle(
                    color: Color.fromRGBO(255, 153, 29, 1.0),
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0),
              ),
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        estado,
                        style: TextStyle(
                            color: Colors.grey.shade700,
                            fontFamily: 'CenturyGothic',
                            fontWeight: FontWeight.bold,
                            fontSize: 10.0),
                      ),
                    ]),
              )
            ],
          ),
        ),
      ),
      onTap: () {
        showSimpleCustomDialog(context, texto, fecha, id, imagen);
      },
    );
  }

  Widget _camposFormulario(
      String texto, TextEditingController _controller, TextInputType type) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(10, 4, 10, 4),
        height: 40,
        child: TextField(
          controller: _controller,
          autofocus: false,
          keyboardType: type,
          style: new TextStyle(
            fontSize: 13.0,
            color: Colors.black,
            fontFamily: 'CenturyGothic',
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.justify,
          decoration: new InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(233, 233, 233, 1),
            hintText: texto,
            contentPadding: EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
            focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius: new BorderRadius.circular(5),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius: new BorderRadius.circular(5),
            ),
          ),
        ));
  }

  Widget _camposFormulario2(
      String texto, TextEditingController _controller, TextInputType type) {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.47,
        height: 30,
        child: TextField(
          controller: _controller,
          autofocus: false,
          keyboardType: type,
          style: new TextStyle(
            fontSize: 13.0,
            color: Colors.black,
            fontFamily: 'CenturyGothic',
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.justify,
          decoration: new InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(233, 233, 233, 1),
            hintText: texto,
            contentPadding:
                const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
            focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius: new BorderRadius.circular(5),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius: new BorderRadius.circular(5),
            ),
          ),
        ));
  }

  Widget _camposFormulario3(
      String texto, TextEditingController _controller, TextInputType type) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(10, 4, 10, 4),
        height: 40,
        child: TextField(
          controller: _controller,
          autofocus: false,
          keyboardType: type,
          style: new TextStyle(
            fontSize: 13.0,
            color: Colors.black,
            fontFamily: 'CenturyGothic',
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.justify,
          decoration: new InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(233, 233, 233, 1),
            hintText: texto,
            contentPadding:
                EdgeInsets.only(left: 14.0, bottom: 4.0, top: 4.0, right: 14.0),
            focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white, width: 0.0),
              borderRadius: new BorderRadius.circular(5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white, width: 0.0),
              borderRadius: new BorderRadius.circular(5),
            ),
          ),
        ));
  }

  Widget _qr(String _codigo, String visible) {
    final size = MediaQuery.of(context).size;
    return Container(
        padding: const EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            borderRadius: visible == "si"
                ? BorderRadius.circular(8)
                : BorderRadius.circular(0),
            border: Border.all(
                width: 8,
                color: visible == "si"
                    ? Colors.orange
                    : Color.fromRGBO(0, 0, 0, 0.1)
                // color: Color.fromRGBO(255, 153, 29, 1.0)
                )),
        child: QrImage(
          backgroundColor: Colors.white,
          data: _codigo,
          version: QrVersions.auto,
          size: visible == "si" ? size.width * 0.50 : size.width * 0.315,
          gapless: false,
          embeddedImageStyle: QrEmbeddedImageStyle(
            size: Size(80, 80),
          ),
        ));
  }

  Widget _campoAlert(String label, String texto) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(10.0, 4.0, 10, 4.0),
        height: 46,
        child: TextFormField(
          initialValue: texto,
          autofocus: false,
          enabled: false,
          style: new TextStyle(
            fontSize: 13.0,
            color: Colors.black,
            fontFamily: 'CenturyGothic',
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.justify,
          decoration: new InputDecoration(
            filled: true,
            labelText: label,
            fillColor: Color.fromRGBO(233, 233, 233, 1),
            contentPadding:
                EdgeInsets.only(left: 10.0, bottom: 4.0, top: 4.0, right: 10.0),
            focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white, width: 0.0),
              borderRadius: new BorderRadius.circular(5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white, width: 0.0),
              borderRadius: new BorderRadius.circular(5),
            ),
          ),
        ));
  }

  void showSimpleCustomDialog(BuildContext context, String nombrelista,
      String fecha, String id, imagenvisita) {
    Widget imagenvisitaWID;
    try {
      print(imagenvisita);
      if (imagenvisita == null) {
        imagenvisitaWID = Center();
      } else {
        imagenvisitaWID = GestureDetector(
          onTap: () => {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => HeroPhotoViewRouteWrapper(
                          imageProvider: NetworkImage(
                              urlImage + 'storage/' + imagenvisita),
                        )))
          },
          child:
              Image(image: NetworkImage(urlImage + 'storage/' + imagenvisita)),
        );
      }
    } catch (a) {
      imagenvisitaWID =
          Image(image: AssetImage("recursos/imagenes/profile.png"));
    }

    final size = MediaQuery.of(context).size;
    Dialog simpleDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: RepaintBoundary(
          // key: _globalKey,
          child: Container(
        decoration: BoxDecoration(
            border:
                Border.all(width: 8, color: Color.fromRGBO(255, 153, 29, 1.0)),
            color: Colors.white),
        width: size.width * 1.18,
        height: size.width * 1.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text("VISITA",
                  style: TextStyle(
                      fontSize: 24.0,
                      color: Color.fromRGBO(205, 105, 55, 1.0),
                      fontFamily: 'CenturyGothic',
                      fontWeight: FontWeight.bold)),
            ),
            Padding(padding: EdgeInsets.only(bottom: 10)),
            _campoAlert("Nombre", nombrelista),
            _campoAlert("Fecha", fecha),
            _campoAlert("Cedula", id),
            Padding(padding: EdgeInsets.only(bottom: 5)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: size.width * 0.34,
                  height: size.width * 0.14,
                  color: Colors.grey.shade200,
                  child: imagenvisitaWID,
                ),
                Padding(padding: EdgeInsets.only(right: 10)),
                _qr(id + "&" + fecha, "no"),
              ],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            ),
            botonCompartir(fecha, 'si')
          ],
        ),
      )),
    );
    Widget imageCompartir() {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Opacity(
          opacity: 1,
          child: RepaintBoundary(
              key: _globalKey,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 8, color: Color.fromRGBO(255, 153, 29, 1.0)),
                    color: Colors.white),
                width: size.width * 1.38,
                height: size.width * 1.2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Text("VISITA GENERADA",
                          style: TextStyle(
                              fontSize: 24.0,
                              color: Color.fromRGBO(255, 114, 0, 1),
                              fontWeight: FontWeight.bold)),
                    ),
                    Center(
                      child: Text("MEDIANTE",
                          style: TextStyle(
                              fontSize: 18.0,
                              color: Color.fromRGBO(11, 10, 68, 1),
                              fontWeight: FontWeight.bold)),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                    Container(
                        width: size.width * 0.44,
                        child: Image.asset('recursos/logo/logo.png')),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                    _qr(id + "&" + fecha, "si"),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                    // botonCompartir(fecha, 'si'),
                    Container(
                      width: size.width * 0.54,
                      child: Image(
                        image: AssetImage('recursos/imagenes/LogoSF.png'),
                      ),
                    ),
                  ],
                ),
              )),
        ),
      );
    }

    showDialog(
        context: context,
        builder: (BuildContext context) => Stack(
              children: [
                imageCompartir(),
                simpleDialog,
              ],
            ));
  }

  void showSimpleCustomDialogpqr(BuildContext context, String code) {
    Dialog simpleDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: RepaintBoundary(
        key: _globalKey,
        child: Container(
          color: Colors.white,
          margin: EdgeInsets.all(3.0),
          width: 500,
          height: 500,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(
                  "VISITA GENERADA CORRECTAMENTE",
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Color.fromRGBO(255, 153, 29, 1.0),
                    fontFamily: 'CenturyGothic',
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 10)),
              Icon(
                Icons.check_circle_outline,
                color: Color.fromRGBO(255, 153, 29, 1.0),
                size: 50.0,
              ),
              Container(
                width: 300,
                height: 300,
                decoration: BoxDecoration(
                  border: Border.all(
                      width: 8, color: Color.fromRGBO(255, 153, 29, 1.0)),
                ),
                child: QrImage(
                  data: code,
                  version: QrVersions.auto,
                  size: 150,
                  gapless: false,
                  embeddedImageStyle: QrEmbeddedImageStyle(
                    size: Size(80, 80),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
              ),
              botonCompartir(_selectedDate, 'no')
            ],
          ),
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => simpleDialog);
  }

  Widget _calendario() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // Date
        Container(
          decoration: BoxDecoration(
              border: Border(
                top: BorderSide(width: 1.0, color: Colors.grey),
                left: BorderSide(width: 1.0, color: Colors.grey),
                right: BorderSide(width: 1.0, color: Colors.grey),
                bottom: BorderSide(width: 1.0, color: Colors.grey),
              ),
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Builder(
                  builder: (context) => InkWell(
                    child: Text(
                      _selectedDate,
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        fontFamily: 'CenturyGothic',
                      ),
                    ),
                    onTap: () {
                      _selectDate(context);
                    },
                  ),
                ),
                Builder(
                  builder: (context) => IconButton(
                    icon: Icon(Icons.calendar_today),
                    tooltip: 'Tap to open date picker',
                    onPressed: () {
                      _selectDate(context);
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(width: 10.0),
        // Time
        Container(
          decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(width: 1.0, color: Colors.grey),
                left: BorderSide(width: 1.0, color: Colors.grey),
                right: BorderSide(width: 1.0, color: Colors.grey),
                bottom: BorderSide(width: 1.0, color: Colors.grey),
              ),
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Padding(
            padding: const EdgeInsets.only(left: 4.0, right: 4.0),
            child: Builder(
              builder: (context) => IconButton(
                icon: Icon(Icons.av_timer),
                tooltip: 'Tap to open date picker',
                onPressed: () {
                  _selectTime(context);
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  botonCompartir(fecha, String visible) {
    return GestureDetector(
      onTap: () {
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DrawerItem() ));
      },
      child: Container(
          width: double.infinity,
          margin: EdgeInsets.symmetric(horizontal: 30.0),
          padding: EdgeInsets.symmetric(vertical: 7.0),
          child: RaisedButton(
            color: visible == "si" ? Colors.orange : Colors.grey,
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5),
            ),
            onPressed: () {
              _capturePng(fecha);
            },
            child: Text(
              'COMPARTIR',
              style: TextStyle(
                  fontSize: 15.0,
                  color: visible == "si"
                      ? Colors.white
                      : Color.fromRGBO(255, 153, 29, 1.0),
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          )),
    );
  }

  _capturePng(fecha) async {
    try {
      Future.delayed(Duration(milliseconds: 100), () async {
        RenderRepaintBoundary boundary =
            _globalKey.currentContext.findRenderObject();

        ui.Image image = await boundary.toImage(pixelRatio: 2);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);
        Uint8List pngBytes = byteData.buffer.asUint8List();
        //_selectedDate
        Share.file("asd", "qr.png", pngBytes, "image/png",
            text:
                "Visita apartada, fecha y  hora ${fecha}- Presentar QR en Porteria al momento de Tu visita.- Visita Generada desde el APP 'Klegus' ");
      });
    } catch (a) {
      _alertVisitaMensajes(context, a.toString());
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    datetime = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime.now().subtract(Duration(days: 1)),
        lastDate: _date.add(new Duration(days: 360)),
        locale: Locale('es', 'CO'));

    if (datetime != null) {
      setState(() {
        _date = datetime;

        DateTime _datetime = DateTime(
            _date.year, _date.month, _date.day, _time.hour, _time.minute);
        _selectedDate = dateFormat.format(_datetime);
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    picked = await showTimePicker(
      context: context,
      initialTime: _time,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
          child: child,
        );
      },
    );

    if (picked != null) {
      setState(() {
        _time = picked;
        DateTime _datetime = DateTime(
            _date.year, _date.month, _date.day, _time.hour, _time.minute);
        _selectedDate = dateFormat.format(_datetime);
      });
    }
  }

  bool _validarnumeros(String value) {
    final n = num.tryParse(value);
    if (n == null) {
      return false;
    }
    return true;
  }
}
